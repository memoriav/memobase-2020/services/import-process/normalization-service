## Normalization Service

This service is responsible to manage the normalization and transformation of data. This normalization
is done based on configurations and mappings.

A mapping can by applied by supplying a csv list which then can map any source value to a specific normalized value.
The normalized value is added as a new entity and the old value is kept in place.

All new values are referenced by an activity entity which states when and from what this new entity was created.

[Confluence Doku](https://ub-basel.atlassian.net/wiki/spaces/MEMOBASE/pages/29295489/Service+RDF-Normalizer)
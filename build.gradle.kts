plugins {
    application
    jacoco
    // https://plugins.gradle.org/plugin/org.jetbrains.kotlin.jvm
    kotlin("jvm") version "2.0.20"
    // https://plugins.gradle.org/plugin/org.jetbrains.kotlin.plugin.serialization
    id("org.jetbrains.kotlin.plugin.serialization") version "2.0.20"
    // https://plugins.gradle.org/plugin/io.freefair.git-version
    id("io.freefair.git-version") version "8.10"
    // https://plugins.gradle.org/plugin/org.jetbrains.dokka
    id("org.jetbrains.dokka") version "1.9.20"
}

group = "ch.memobase"

application {
    mainClass.set("ch.memobase.App")
    tasks.withType<Tar>().configureEach {
        archiveFileName = "app.tar"
    }
}

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(21)
    }
}


repositories {
    mavenCentral()
    maven {
        setUrl("https://gitlab.switch.ch/api/v4/projects/1324/packages/maven")
    }
}

dependencies {
    // https://gitlab.switch.ch/memoriav/memobase-2020/libraries/memobase-kafka-utils/-/tags
    implementation("ch.memobase:memobase-kafka-utils:0.3.7")
    // https://gitlab.switch.ch/memoriav/memobase-2020/libraries/service-utilities/-/tags
    implementation("ch.memobase:memobase-service-utilities:4.14.1")

    // https://central.sonatype.com/artifact/org.jetbrains.kotlinx/kotlinx-serialization-json
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.7.2")

    // https://central.sonatype.com/artifact/io.github.hakky54/sslcontext-kickstart
    implementation("io.github.hakky54:sslcontext-kickstart:8.3.7")
    // https://central.sonatype.com/artifact/io.github.hakky54/sslcontext-kickstart-for-pem
    implementation("io.github.hakky54:sslcontext-kickstart-for-pem:8.3.7")

    // https://central.sonatype.com/artifact/org.apache.logging.log4j/log4j-api
    implementation("org.apache.logging.log4j:log4j-api:2.23.1")
    // https://central.sonatype.com/artifact/org.apache.logging.log4j/log4j-core
    implementation("org.apache.logging.log4j:log4j-core:2.23.1")
    // https://central.sonatype.com/artifact/org.apache.logging.log4j/log4j-slf4j2-impl
    implementation("org.apache.logging.log4j:log4j-slf4j2-impl:2.23.1")

    // Kafka Imports
    // https://central.sonatype.com/artifact/org.apache.kafka/kafka-streams
    implementation("org.apache.kafka:kafka-streams:3.8.0")
    // RDF Library
    // https://central.sonatype.com/artifact/org.apache.jena/apache-jena
    implementation("org.apache.jena:apache-jena:5.1.0")

    // YAML Parser
    // https://central.sonatype.com/artifact/com.charleskorn.kaml/kaml?smo=true
    implementation("com.charleskorn.kaml:kaml:0.61.0")
    // https://central.sonatype.com/artifact/app.softwork/kotlinx-serialization-csv
    implementation("app.softwork:kotlinx-serialization-csv:0.0.18")
    // CSV Reader
    // https://central.sonatype.com/artifact/com.github.doyaaaaaken/kotlin-csv-jvm
    implementation("com.github.doyaaaaaken:kotlin-csv-jvm:1.9.3")

    // https://central.sonatype.com/artifact/org.junit.jupiter/junit-jupiter
    testImplementation("org.junit.jupiter:junit-jupiter:5.11.0")
    // https://central.sonatype.com/artifact/org.junit.jupiter/junit-jupiter-engine
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.11.0")
    // https://central.sonatype.com/artifact/org.junit.jupiter/junit-jupiter-api
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.11.0")
    // https://central.sonatype.com/artifact/org.assertj/assertj-core
    testImplementation("org.assertj:assertj-core:3.25.3")
    // https://mvnrepository.com/artifact/org.apache.kafka/kafka-streams-test-utils
    testImplementation("org.apache.kafka:kafka-streams-test-utils:3.8.0")
}

configurations {
    all {
        exclude(group = "org.slf4j", module = "slf4j-log4j12")
    }
}

tasks.named<Test>("test") {
    useJUnitPlatform()

    testLogging {
        setEvents(mutableListOf("passed", "skipped", "failed"))
    }
}

tasks.jacocoTestReport {
    dependsOn(tasks.test)
    reports {
        csv.required = true
    }
}

/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.settings.SettingsLoader
import org.apache.logging.log4j.LogManager
import kotlin.system.exitProcess

class App {
    companion object {
        private val log = LogManager.getLogger(this::class.java)
        @JvmStatic fun main(args: Array<String>) {
            try {
                Service().run()
            } catch (ex: Exception) {
                ex.printStackTrace()
                log.error("Stopping application due to error: " + ex.message)
                exitProcess(1)
            }
        }

        fun defineSettings(fileName: String): SettingsLoader {
            return SettingsLoader(
                listOf(
                    Service.TRANSFORM_MAPPING_CONFIG_PROP_NAME,
                    Service.CONFIG_TOPIC_PROP_NAME,
                    Service.REPORTING_STEP_NAME_PROP_NAME,
                    Service.WIKIDATA_SERVICE_URL_PROP_NAME,
                    Service.CLIENT_KEY_PATH_PROP_NAME,
                    Service.CLIENT_CERTIFICATE_PATH_PROP_NAME,
                    Service.CA_CERTIFICATE_PATH_PROP_NAME,
                    Service.WIKIDATA_LINKER_URL_PROP_NAME,
                    Service.CONNECTOM_SERVICE_URL_PROP_NAME,
                    Service.APP_VERSION,
                ),
                fileName,
                useStreamsConfig = true
            )
        }
    }
}
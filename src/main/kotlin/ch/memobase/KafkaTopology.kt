/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.configs.GlobalTransformsLoader
import ch.memobase.configs.LocalTransformsLoader
import ch.memobase.exceptions.InvalidInputException
import ch.memobase.helpers.GlobalTransformException
import ch.memobase.helpers.RequestClient
import ch.memobase.helpers.Values
import ch.memobase.helpers.getSSLContext
import ch.memobase.kafka.utils.ConfigJoiner
import ch.memobase.kafka.utils.models.ImportService
import ch.memobase.kafka.utils.models.JoinedValues
import ch.memobase.rdf.MemobaseModel
import ch.memobase.reporting.Report
import ch.memobase.reporting.ReportStatus
import ch.memobase.schema.pipeline.DataStepOne
import ch.memobase.schema.pipeline.DataStepThree
import ch.memobase.schema.pipeline.DataStepTwo
import ch.memobase.settings.HeaderExtractionSupplier
import ch.memobase.settings.SettingsLoader
import ch.memobase.transform.WikidataLinksEnricher
import org.apache.jena.rdf.model.Model
import org.apache.jena.riot.Lang
import org.apache.jena.riot.RDFDataMgr
import org.apache.jena.riot.RiotException
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.kstream.Branched
import org.apache.kafka.streams.kstream.Named
import org.apache.logging.log4j.LogManager
import java.io.ByteArrayInputStream
import java.io.StringWriter
import kotlin.system.exitProcess

class KafkaTopology(
    private val settings: SettingsLoader,
) {
    private val log = LogManager.getLogger(this::class.java)
    private val reportTopic = settings.processReportTopic
    private val step = settings.appSettings.getProperty(Service.REPORTING_STEP_NAME_PROP_NAME)
    private val stepVersion = settings.appSettings.getProperty(Service.APP_VERSION)
    private val sslContext = getSSLContext(
        certificateAuthorityPath = settings.appSettings.getProperty(Service.CA_CERTIFICATE_PATH_PROP_NAME),
        clientCertificatePath = settings.appSettings.getProperty(Service.CLIENT_CERTIFICATE_PATH_PROP_NAME),
        clientKeyPath = settings.appSettings.getProperty(Service.CLIENT_KEY_PATH_PROP_NAME)
    )
    private val requestClient = RequestClient(
        settings.appSettings.getProperty(Service.WIKIDATA_SERVICE_URL_PROP_NAME),
        settings.appSettings.getProperty(Service.WIKIDATA_LINKER_URL_PROP_NAME),
        settings.appSettings.getProperty(Service.CONNECTOM_SERVICE_URL_PROP_NAME),
        sslContext
    )
    private val globalTransforms = try {
        GlobalTransformsLoader.parse(settings.appSettings.getProperty(Service.TRANSFORM_MAPPING_CONFIG_PROP_NAME))
    } catch (ex: GlobalTransformException) {
        exitProcess(1)
    }

    init {
        require(globalTransforms.isNotEmpty())
    }

    private val configJoiner = ConfigJoiner<String, ByteArray>(
        ImportService.LocalTransform,
        Serdes.String(),
        Serdes.serdeFrom(
            { _, data -> data },
            { _, data -> data }
        ),
        this::parseConfig
    )

    fun prepare(): StreamsBuilder {
        val builder = StreamsBuilder()

        val configStream =
            builder.stream<String, String>(settings.appSettings.getProperty(Service.CONFIG_TOPIC_PROP_NAME))
                .map { key, value -> KeyValue(key.toByteArray(), value.toByteArray()) }

        val stream = builder.stream<String, String>(settings.inputTopic)

        val joinedStream =
            configJoiner.join(stream, configStream)

        val branchedStream = joinedStream
            .split(Named.`as`(Values.KS_CONFIG_NAME))
            .branch(
                { _, value -> value.hasValue() },
                Branched.`as`(Values.KS_CONFIG_VALUE)
            ).defaultBranch(Branched.`as`(Values.KS_CONFIG_EXCEPTION))

        branchedStream["${Values.KS_CONFIG_NAME}${Values.KS_CONFIG_EXCEPTION}"]
            ?.mapValues { value -> value.exception }
            ?.mapValues { readOnlyKey, value ->
                Report(
                    readOnlyKey,
                    ReportStatus.fatal,
                    value.localizedMessage,
                    step,
                    stepVersion,
                ).toJson()
            }
            ?.to(reportTopic)

        val filterInvalidLocalTransforms =
            branchedStream["${Values.KS_CONFIG_NAME}${Values.KS_CONFIG_VALUE}"]
                ?.mapValues { value -> value.value }
                ?.processValues(HeaderExtractionSupplier<JoinedValues<String, ByteArray>>())
                ?.mapValues { value ->
                    val localTransform = if (value.first.hasRight()) {
                        value.first.right
                    } else {
                        byteArrayOf()
                    }
                    DataStepOne(
                        value.first.left, LocalTransformsLoader(
                            localTransform,
                            step,
                            stepVersion,
                            requestClient,
                        ), value.second
                    )
                }
                ?.mapValues { readOnlyKey, value ->
                    val report = value.localTransformLoader.parse(readOnlyKey)
                    DataStepTwo(value.message, value.localTransformLoader.get(), report, value.metadata)
                }
                ?.split(Named.`as`(Values.KS_TRANSFORM_NAME))
                ?.branch(
                    { _, value -> value.localTransformsReport.status == ReportStatus.fatal },
                    Branched.`as`(Values.KS_TRANSFORM_FATAL),
                )
                ?.defaultBranch(Branched.`as`(Values.KS_TRANSFORM_SUCCESS))

        filterInvalidLocalTransforms?.get("${Values.KS_TRANSFORM_NAME}${Values.KS_TRANSFORM_FATAL}")
            ?.mapValues { value ->
                value.localTransformsReport.toJson()
            }
            ?.to(reportTopic)

        val parsedModelStream =
            filterInvalidLocalTransforms?.get("${Values.KS_TRANSFORM_NAME}${Values.KS_TRANSFORM_SUCCESS}")
                ?.mapValues { value -> createModel(value) }
                ?.split(Named.`as`(Values.KS_MODEL_NAME))
                ?.branch(
                    { _, value -> value != null },
                    Branched.`as`(Values.KS_MODEL_SUCCESS)
                )
                ?.defaultBranch(Branched.`as`(Values.KS_MODEL_FATAL))

        parsedModelStream?.get("${Values.KS_MODEL_NAME}${Values.KS_MODEL_FATAL}")
            ?.mapValues { key, _ ->
                Report(
                    key,
                    ReportStatus.fatal,
                    "Could not parse message. Found invalid input data (RiotException). Check logs.",
                    step,
                    stepVersion,
                ).toJson()
            }
            ?.to(reportTopic)

        val finishedStream = parsedModelStream?.get("${Values.KS_MODEL_NAME}${Values.KS_MODEL_SUCCESS}")
            ?.mapValues { key, value -> transformations(key, value!!) }
            ?.mapValues { value -> Pair(value.first, writeModel(value.second)) }

        finishedStream
            ?.filterNot { _, value -> value.first.status == ReportStatus.fatal }
            ?.mapValues { value -> value.second }
            ?.to(settings.outputTopic)

        finishedStream
            ?.mapValues { value -> value.first.toJson() }
            ?.to(reportTopic)

        return builder
    }

    private fun createModel(input: DataStepTwo): DataStepThree? {
        val model = MemobaseModel()
        try {
            RDFDataMgr.read(model, ByteArrayInputStream(input.data.toByteArray()), Lang.NTRIPLES)
        } catch (ex: RiotException) {
            return null
        }
        return DataStepThree(model, input.localTransforms, input.localTransformsReport, input.headerMetadata)
    }

    private fun transformations(key: String, input: DataStepThree): Pair<Report, Model> {
        val transformConfigs = input.localTransforms + globalTransforms
        val reportMessages = mutableListOf<String>()
        val record = input.model.getRecord()
        for (transformConfig in transformConfigs) {
            val listOfResources = input.model.listRicoResourceSubjects().toList()
            if (transformConfig is WikidataLinksEnricher) {
                transformConfig.enrichFreetextEntities(record, input.model)
            }

            for (resource in listOfResources) {
                try {
                    reportMessages.addAll(transformConfig.transform(resource, record, input.model))
                } catch (ex: InvalidInputException) {
                    log.error(ex.localizedMessage)
                    Pair(
                        Report(
                            key,
                            ReportStatus.fatal,
                            "InvalidInput: ${ex.localizedMessage}\n${input.localTransformsReport.message}",
                            step,
                            stepVersion,
                        ),
                        input.model
                    )
                } catch (ex: Exception) {
                    log.error("Unexpected Exception: $ex")
                    Pair(
                        Report(
                            key,
                            ReportStatus.fatal,
                            "${ex.javaClass.name}: ${ex.localizedMessage}\n${input.localTransformsReport.message}",
                            step,
                            stepVersion,
                        ),
                        input.model
                    )
                }
            }
        }
        var reportMessage = if (reportMessages.isEmpty()) "" else reportMessages.reduce { acc, s -> acc + "\n" + s }
        reportMessage += input.localTransformsReport.message
        reportMessage = reportMessage.trim()
        return Pair(
            Report(
                key,
                if (reportMessages.isEmpty()) ReportStatus.success else ReportStatus.warning,
                reportMessage,
                step,
                stepVersion,
            ),
            input.model
        )
    }

    private fun writeModel(data: Model): String {
        val out = StringWriter()
        RDFDataMgr.write(out, data, Lang.NTRIPLES)
        return out.toString().trim()
    }

    private fun parseConfig(data: ByteArray): ByteArray {
        return data
    }
}
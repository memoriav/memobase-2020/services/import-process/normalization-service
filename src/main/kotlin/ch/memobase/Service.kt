/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import org.apache.kafka.streams.KafkaStreams
import org.apache.logging.log4j.LogManager

class Service {
    companion object {
        const val CA_CERTIFICATE_PATH_PROP_NAME = "caCertificatePath"
        const val CLIENT_CERTIFICATE_PATH_PROP_NAME = "clientCertificatePath"
        const val CLIENT_KEY_PATH_PROP_NAME = "clientKeyPath"
        const val WIKIDATA_SERVICE_URL_PROP_NAME = "wikidataServiceUrl"
        const val WIKIDATA_LINKER_URL_PROP_NAME = "wikidataLinkerUrl"
        const val CONNECTOM_SERVICE_URL_PROP_NAME = "connectomServiceUrl"
        const val TRANSFORM_MAPPING_CONFIG_PROP_NAME = "transformMapping"
        const val CONFIG_TOPIC_PROP_NAME = "configTopic"
        const val REPORTING_STEP_NAME_PROP_NAME = "reportingStepName"
        const val APP_VERSION = "appVersion"
    }

    private val log = LogManager.getLogger(this::class.java)
    private val settings = App.defineSettings("app.yml")

    fun run() {
        val stream = KafkaStreams(KafkaTopology(settings).prepare().build(), settings.kafkaStreamsSettings)
        stream.use {
            it.start()
            while (stream.state().isRunningOrRebalancing) {
                Thread.sleep(10_000L)
            }
            log.info("Shutting application down now...")
            throw Exception("Stream stopped running!")
        }
    }
}
/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.configs

import ch.memobase.exceptions.InvalidMappingException
import ch.memobase.helpers.GlobalTransformException
import ch.memobase.helpers.ValidationError
import ch.memobase.schema.global.GlobalTransform
import ch.memobase.transform.ITransformer
import com.charleskorn.kaml.Yaml
import com.charleskorn.kaml.decodeFromStream
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.MissingFieldException
import org.apache.logging.log4j.LogManager
import java.io.File


/**
 *  Utility object to load the global transform configuration and initialize the transformer instances from it.
 */
object GlobalTransformsLoader {
    private val log = LogManager.getLogger(this::class.java)

    /**
     * Parses the transform at the configuration and initializes all the defined transformers.
     */
    @OptIn(ExperimentalSerializationApi::class)
    fun parse(configuration: String): List<ITransformer> {
        val file = File(configuration)
        if (!file.exists()) {
            throw GlobalTransformException("Configuration file does not exists: $configuration.")
        }
        val globalTransform = file.inputStream().use { inputStream ->
            try {
                Yaml.default.decodeFromStream<GlobalTransform>(inputStream)
            } catch (ex: MissingFieldException) {
                throw GlobalTransformException("Mandatory field in configuration is missing: ${ex.localizedMessage}.")
            }
        }
        val transforms = mutableListOf<ITransformer>()
        try {
            globalTransform.let {
                it.normalizeDate.let { date ->
                    if (date != null)
                        transforms.add(date.generate())
                }
                it.normalizeCarrierType.let { carrierType ->
                    if (carrierType != null) {
                        transforms.add(carrierType.generate())
                    }
                }
                it.normalizeLanguages.let { normalizeLanguages ->
                    if (normalizeLanguages != null) {
                        transforms.add(normalizeLanguages.generate())
                    }
                }
                it.normalizeGenre.let { normalizeGenre ->
                    if (normalizeGenre != null) {
                        transforms.add(normalizeGenre.generate())
                    }
                }
            }
        } catch (ex: InvalidMappingException) {
            log.error("Invalid Mapping: ${ex.localizedMessage}")
            throw GlobalTransformException("Invalid Mapping: ${ex.localizedMessage}")
        } catch (ex: ValidationError) {
            log.error("Validation Error: ${ex.localizedMessage}")
            throw GlobalTransformException("Validation Error: ${ex.localizedMessage}")
        } catch (ex: Exception) {
            log.error("${ex}: ${ex.localizedMessage}")
            throw GlobalTransformException("${ex}: ${ex.localizedMessage}")
        }
        if (transforms.isEmpty()) {
            log.error("Empty configuration: $configuration!")
            throw GlobalTransformException("Empty configuration: $configuration!")
        }
        return transforms
    }
}
/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.configs

import ch.memobase.helpers.RequestClient
import ch.memobase.helpers.ValidationError
import ch.memobase.schema.local.LocalTransform
import ch.memobase.reporting.Report
import ch.memobase.reporting.ReportStatus
import ch.memobase.transform.ITransformer
import com.charleskorn.kaml.IncorrectTypeException
import com.charleskorn.kaml.Yaml
import com.charleskorn.kaml.decodeFromStream
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.MissingFieldException
import org.apache.logging.log4j.LogManager
import java.io.ByteArrayInputStream
import java.util.regex.PatternSyntaxException

class LocalTransformsLoader(
    private val data: ByteArray,
    private val step: String,
    private val stepVersion: String,
    private val requestClient: RequestClient,
) {
    private val log = LogManager.getLogger(this::class.java)
    private val transforms = mutableListOf<ITransformer>()

    @OptIn(ExperimentalSerializationApi::class)
    fun parse(key: String): Report {
        return try {
            if (data.isEmpty())
                Report(
                    key,
                    ReportStatus.ignored,
                    "[Local Transform] Ignored empty local transformation file.",
                    step,
                    stepVersion,
                )
            else {
                val byteArrayInputStream = ByteArrayInputStream(data)
                val localTransform = byteArrayInputStream.use { inputStream ->
                    try {
                        Yaml.default.decodeFromStream<LocalTransform>(inputStream)
                    } catch (ex: MissingFieldException) {
                        log.error(ex.message, ex)
                        return Report(
                            key,
                            ReportStatus.fatal,
                            "[Local Transform] MissingFieldException: ${ex.localizedMessage}",
                            step,
                            stepVersion,
                        )
                    } catch (ex: IncorrectTypeException) {
                        log.error(ex.message, ex)
                        return Report(
                            key,
                            ReportStatus.fatal,
                            "[Local Transform] YamlParseError (IncorrectTypeException): ${ex.localizedMessage}",
                            step,
                            stepVersion,
                        )
                    }
                }

                localTransform.splitEntity?.forEach {
                    transforms.add(it.generate())
                }
                localTransform.normalizePerson?.generate().let {
                    if (it != null) {
                        transforms.addAll(it)
                    }
                }
                localTransform.languageTagEnricher?.generate().let {
                    if (it != null) {
                        transforms.add(it)
                    }
                }
                localTransform.wikidataEnricher?.generate(
                    requestClient = requestClient
                ).let {
                    if (it != null) {
                        transforms.add(it)
                    }
                }
                Report(
                    key, ReportStatus.success, "", step,
                    stepVersion,
                )
            }
        } catch (ex: ValidationError) {
            val message = "[Local Transform] ValidationError: ${ex.localizedMessage}"
            log.error(message)
            Report(
                key, ReportStatus.fatal, message, step,
                stepVersion,
            )
        } catch (ex: PatternSyntaxException) {
            val message = "[Local Transform] RegexError: ${ex.localizedMessage}"
            log.error(message)
            Report(
                key, ReportStatus.fatal, message, step,
                stepVersion,
            )
        } catch (ex: Exception) {
            val message = "[Local Transform] ${ex.javaClass.name}: ${ex.localizedMessage}"
            log.error(message)
            Report(
                key, ReportStatus.fatal, message, step,
                stepVersion,
            )
        }
    }

    fun get(): List<ITransformer> {
        return transforms
    }
}
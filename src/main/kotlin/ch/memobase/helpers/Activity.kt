/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.helpers

import ch.memobase.rdf.MemobaseModel
import ch.memobase.rdf.RDF
import ch.memobase.rdf.RICO
import ch.memobase.rdf.RicoResource
import org.apache.jena.datatypes.xsd.impl.XSDDateTimeType
import org.apache.jena.rdf.model.Literal
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Resource
import org.apache.jena.rdf.model.ResourceFactory
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

/**
 * Utility functions to create activity resources.
 */
object Activity {

    private const val ACTIVITY_TYPE = "enrichment"

    /**
     * Add an activity to a resource.
     *
     * @param createdResource The resource that is created by the activity.
     * @param mechanismName The name of the mechanism that performs the activity.
     * @param sourceResource The resource that the new resource is created from.
     * @param model The model the resources are a part of.
     */
    fun appendActivity(
        createdResource: RicoResource,
        mechanismName: String,
        sourceResource: RicoResource,
        model: MemobaseModel
    ) {
        val mechanism = createMechanism(model, mechanismName)
        val activity = createActivity(model)
        activity.addProperty(RICO.isOrWasPerformedBy, mechanism)
        mechanism.addProperty(RICO.performsOrPerformed, activity)
        activity.addProperty(RICO.affectsOrAffected, sourceResource.resource)
        sourceResource.resource.addProperty(RICO.isOrWasAffectedBy, activity)
        createdResource.addProperty(RICO.resultsOrResultedFrom, activity)
        activity.addProperty(RICO.resultsOrResultedIn, createdResource.resource)
    }

    /**
     * Create an activity with a specific mechanism name in the current model.
     *
     * @param mechanismName The name of the mechanism to be created.
     * @param model The current model.
     *
     * @return The [Resource] reference to the activity node.
     */
    fun createActivity(
        mechanismName: String,
        model: MemobaseModel
    ): Resource {
        val mechanism = createMechanism(model, mechanismName)
        val activity = createActivity(model)
        activity.addProperty(RICO.isOrWasPerformedBy, mechanism)
        mechanism.addProperty(RICO.performsOrPerformed, activity)
        return activity
    }

    private fun createMechanism(model: Model, name: String): Resource {
        return model.createResource()
            .addProperty(RDF.type, RICO.Mechanism)
            .addProperty(RICO.name, name)
    }

    private fun createActivity(model: Model): Resource {
        return model.createResource()
            .addProperty(RDF.type, RICO.Activity)
            .addProperty(RICO.type, ACTIVITY_TYPE)
            .addLiteral(RICO.beginningDate, now())
            .addLiteral(RICO.endDate, now())
    }

    private fun now(): Literal {
        return ResourceFactory
            .createTypedLiteral(
                ZonedDateTime
                    .now(ZoneId.of("UTC"))
                    .format(DateTimeFormatter.ofPattern("YYYY-MM-dd'T'HH:mm:ss'Z'")).toString(),
                XSDDateTimeType("dateTime")
            )
    }

}
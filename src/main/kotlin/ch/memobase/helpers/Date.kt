/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.helpers

import java.text.DateFormatSymbols
import java.util.Locale

/**
 * Utility object for date processing
 */
object Date {
    private val germanDateSymbols = DateFormatSymbols.getInstance(Locale.GERMAN)
    private val frenchDateSymbols = DateFormatSymbols.getInstance(Locale.FRENCH)
    private val italianDateSymbols = DateFormatSymbols.getInstance(Locale.ITALIAN)

    const val DAY_GROUP = "day"
    const val MONTH_GROUP = "month"
    const val YEAR_GROUP = "year"
    const val QUALIFIER_GROUP = "qualifier"
    const val CERTAINTY_GROUP = "certainty"

    const val SINGLE_YEAR_GROUP = "singleYear"
    const val SINGLE_MONTH_GROUP = "singleMonth"
    const val DAY_FROM_GROUP = "fromDay"
    const val DAY_UNTIL_GROUP = "untilDay"
    const val MONTH_FROM_GROUP = "fromMonth"
    const val MONTH_UNTIL_GROUP = "untilMonth"
    const val YEAR_FROM_GROUP = "fromYear"
    const val YEAR_UNTIL_GROUP = "untilYear"

    private val monthSymbolsByMonth = run {
        val base = (1..12).associateWith { i ->
            mutableListOf(
                germanDateSymbols.months[i - 1].lowercase(),
                germanDateSymbols.shortMonths[i - 1].lowercase(),
                frenchDateSymbols.months[i - 1].lowercase(),
                frenchDateSymbols.shortMonths[i - 1].lowercase(),
                italianDateSymbols.months[i - 1].lowercase(),
                italianDateSymbols.shortMonths[i - 1].lowercase()
            )
        }
        // Add month names that are not covered by the base.
        // base[8]?.add("New Month Name")
        base[9]?.add("sept")
        base
    }

    /**
     * The result type of the date normalization.
     *
     * @property date The normalized date value. This value is empty on failure.
     * @property success Was the normalization successful?
     * @property message Explains what went wrong, when it failed. This value is empty on success.
     */
    data class Result(
        val date: String,
        val success: Boolean,
        val message: String,
    )

    /**
     * Takes an integer and checks if it's a valid month.
     */
    private fun normalizeMonthValue(monthAsNumber: Int): Result {
        when (monthAsNumber) {
            in 1..9 -> {
                return Result(
                    date = "0$monthAsNumber",
                    success = true,
                    message = "",
                )
            }
            in 10..12 -> {
                return Result(
                    date = monthAsNumber.toString(),
                    success = true,
                    message = "",
                )
            }
            else -> {
                return Result(
                    date = "",
                    success = false,
                    message = "Value $monthAsNumber is not a valid month number.",
                )
            }
        }
    }

    /**
     * Takes an input value and validates if it is a valid month value. This can be a number from 1 to 12 or
     * the names of months in German, French or Italian. The names are defined in the language specific locale.
     * They can be a shortened or the full name.
     *
     * @param input a month value.
     *
     * @return [Result]
     */
    fun normalizeMonthValue(input: String): Result {
        val monthAsNumber = input.toIntOrNull()
        if (monthAsNumber != null) {
            return normalizeMonthValue(monthAsNumber)
        }
        else {
            monthSymbolsByMonth.entries.forEach { entry ->
                if (entry.value.contains(input.lowercase())) {
                    return normalizeMonthValue(entry.key)
                }
            }
            return Result(
                date = "",
                success = false,
                message = "Could not match value $input to a valid month.",
            )
        }
    }

    private fun normalizeDayValue(dayAsNumber: Int): Result {
        when (dayAsNumber) {
            in 1..9 -> {
                return Result(
                    date = "0$dayAsNumber",
                    success = true,
                    message = "",
                )
            }
            in 10..31 -> {
                return Result(
                    date = "$dayAsNumber",
                    success = true,
                    message = "",
                )
            }
            else -> {
                return Result(
                    date = "",
                    success = false,
                    message = "Could not match value $dayAsNumber as a valid day.",
                )
            }
        }
    }

    fun normalizeDayValue(input: String): Result {
        val dayAsNumber = input.toIntOrNull()
        return if (dayAsNumber != null) {
            normalizeDayValue(dayAsNumber)
        } else {
            Result(
                date = "",
                success = false,
                message = "Could not match value $input to a valid day.",
            )
        }
    }
}

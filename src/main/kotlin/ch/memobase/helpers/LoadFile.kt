/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.helpers

import ch.memobase.schema.Facets
import ch.memobase.schema.Labels
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import java.io.File
import java.util.*

object LoadFile {
    private val csv = csvReader()

    fun readFacetList(path: String): Map<String, Facets> {
        val facetMap = mutableMapOf<String, Facets>()
        val facetList = csv.readAll(File(path))
        for (row in facetList.listIterator(1)) {
            try {
                facetMap[row[0].trim().lowercase(Locale.getDefault())] = Facets(
                    row.subList(1, 6).filter { it != "" }.map { it.trim() },
                    row.subList(7, 12).filter { it != "" }.map { it.trim() }
                )
            } catch (e: IndexOutOfBoundsException) {
                throw ValidationError("Invalid row in csv $path: ${e.message} for row ${row.joinToString(",")}")
            }
        }
        return facetMap
    }

    fun readLabelFile(path: String): Map<String, Labels> {
        val labelsMap = mutableMapOf<String, Labels>()
        val labelList = csv.readAll(File(path))
        for (row in labelList.listIterator(1)) {
            try {
                labelsMap[row[0].trim()] = Labels(row[1].trim(), row[2].trim(), row[3].trim())
            } catch (e: IndexOutOfBoundsException) {
                throw ValidationError("Invalid row in csv $path: ${e.message} for row ${row.joinToString(",")}")
            }
        }
        return labelsMap
    }
}

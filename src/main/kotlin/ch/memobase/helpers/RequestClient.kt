/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.helpers

import ch.memobase.schema.external.*
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromStream
import org.apache.logging.log4j.LogManager
import java.io.IOException
import java.net.ConnectException
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.nio.channels.UnresolvedAddressException
import java.security.cert.CertPathBuilderException
import java.time.Duration
import javax.net.ssl.SSLContext
import kotlin.system.exitProcess

class RequestClient(
    private val wikidataServiceUrl: String,
    private val wikidataLinkerUrl: String,
    private val connectomServiceUrl: String,
    sslContext: SSLContext,
) {
    private val log = LogManager.getLogger(this::class.java)

    private val json = Json {
        ignoreUnknownKeys = false
    }

    private val wikidataLinkerHealthUrl = URI.create("${wikidataLinkerUrl.substringBeforeLast("/")}/health")
    private val connectomServiceHealthUrl = URI.create("${connectomServiceUrl.substringBeforeLast("/")}/health")

    private val wikidataLinkerClient = HttpClient.newBuilder()
        .sslContext(sslContext)
        .version(HttpClient.Version.HTTP_2)
        .connectTimeout(Duration.ofSeconds(100))
        .build()

    private val graphQlServiceClient = HttpClient.newBuilder()
        .sslContext(sslContext)
        .version(HttpClient.Version.HTTP_2)
        .connectTimeout(Duration.ofSeconds(100))
        .build()

    private val wikidataServiceClient = HttpClient.newBuilder()
        .version(HttpClient.Version.HTTP_2)
        .connectTimeout(Duration.ofSeconds(100))
        .build()

    init {
        checkHealth()
    }

    fun checkHealth() {
        if (wikidataLinkerUrl == "none") {
            log.info("Skipping health check as no link is given.")
            return
        }
        log.info("Checking health of wikidata linker: $wikidataLinkerHealthUrl.")
        val health = HttpRequest.newBuilder(wikidataLinkerHealthUrl).GET().build()
        try {
            val responseWikidataLinker =
                wikidataLinkerClient.send(health, java.net.http.HttpResponse.BodyHandlers.ofString())
            if (responseWikidataLinker.statusCode() == 200) {
                log.info("Wikidata Linker is healthy.")
            } else {
                log.error("Wikidata Linker is not healthy.")
                exitProcess(1)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            exitProcess(1)
        }

        log.info("Checking health of connectom service: $connectomServiceHealthUrl.")
        val connectomServiceHealth = HttpRequest.newBuilder(connectomServiceHealthUrl).GET().build()
        try {
            val responseConnectomServiceHealth =
                wikidataLinkerClient.send(connectomServiceHealth, java.net.http.HttpResponse.BodyHandlers.ofString())
            if (responseConnectomServiceHealth.statusCode() == 200) {
                log.info("Connectom Service is healthy.")
            } else {
                log.error("Connectom Service is not healthy.")
                exitProcess(1)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            exitProcess(1)
        }
    }

    @OptIn(ExperimentalSerializationApi::class)
    fun requestWikidataLink(
        value: String,
        recordSetId: String,
        requestType: WikidataLinkerValueType,
    ): WikidataLinkerResponseBody {
        val requestBody = WikidataLinkerRequestBody(
            value, requestType, recordSetId, true
        )
        val request = HttpRequest.newBuilder()
            .uri(URI.create(wikidataLinkerUrl))
            .header("Content-Type", "application/json")
            .header("Accept", "application/json")
            .POST(
                HttpRequest.BodyPublishers.ofString(
                    json.encodeToString(
                        WikidataLinkerRequestBody.serializer(),
                        requestBody
                    )
                )
            )
            .build()

        return try {
            val response = wikidataLinkerClient.send(request, java.net.http.HttpResponse.BodyHandlers.ofInputStream())
            when (response.statusCode()) {
                200 -> {
                    try {
                        return json.decodeFromStream(response.body())
                    } catch (ex: SerializationException) {
                        log.error("Error deserializing processing wikidata linker response: " + ex.message)
                        return WikidataLinkerResponseBody(
                            "An error occurred while processing the response from the Wikidata Linker.",
                            emptyList(),
                            "Error"
                        )
                    }
                }

                404 -> {
                    return WikidataLinkerResponseBody(
                        "No matches found for value $value.",
                        emptyList(),
                        "Error"
                    )
                }

                else -> {
                    log.error("An error occurred while requesting the Wikidata Linker with code ${response.statusCode()}.")
                    return WikidataLinkerResponseBody(
                        "An error occurred while requesting the Wikidata Linker with code ${response.statusCode()}.",
                        emptyList(),
                        "Error"
                    )
                }
            }
        } catch (e: IOException) {
            log.error("Go away from server for $recordSetId.")
            return WikidataLinkerResponseBody(
                "Go away from server for $recordSetId.",
                emptyList(),
                "Error"
            )
        } catch (e: Exception) {
            log.error("Unexpected error happened: ${e.message}.")
            WikidataLinkerResponseBody(
                "An unexpected problem occurred while requesting the wikidata linker: ${e.message}.",
                emptyList(),
                "Error"
            )
        }
    }

    @OptIn(ExperimentalSerializationApi::class)
    fun requestWikidataMetadata(qid: Int, properties: List<String>): WikidataServiceResponseBody {
        val requestBody = WikidataServiceRequestBody(
            false,
            listOf("de", "fr", "it"),
            properties
        )
        val http = HttpRequest.newBuilder()
            .uri(URI.create("$wikidataServiceUrl/wikidata/Q$qid"))
            .header("Content-Type", "application/json")
            .header("Accept", "application/json")
            .POST(
                HttpRequest.BodyPublishers.ofString(
                    json.encodeToString(
                        WikidataServiceRequestBody.serializer(),
                        requestBody
                    )
                )
            )
            .build()

        return try {
            val response = wikidataServiceClient.send(http, java.net.http.HttpResponse.BodyHandlers.ofInputStream())
            when (response.statusCode()) {
                200 -> {
                    try {
                        json.decodeFromStream(response.body())
                    } catch (ex: SerializationException) {
                        log.error("Error processing json content: " + ex.message)
                        WikidataServiceResponseBody.fromError(
                            "Q$qid",
                            "Q$qid",
                            "",
                            "An error occurred while processing the response from the wikidata service for Q$qid."
                        )
                    }
                }

                404 -> {
                    WikidataServiceResponseBody.fromError("Q$qid", "Q$qid", "", "No matches found for value Q$qid.")
                }

                else -> {
                    log.error("An error occurred while requesting the Wikidata service with code ${response.statusCode()}.")
                    WikidataServiceResponseBody.fromError(
                        "Q$qid",
                        "Q$qid",
                        "",
                        "An error occurred while requesting the Wikidata service with code ${response.statusCode()} for Q$qid."
                    )
                }
            }
        } catch (e: UnresolvedAddressException) {
            log.error("Could not resolve address given: $wikidataLinkerUrl.")
            WikidataServiceResponseBody.fromError(
                "Q$qid",
                "Q$qid",
                "",
                "Could not resolve address given: $wikidataLinkerUrl."
            )
        } catch (e: ConnectException) {
            log.error("Could not connect to service.")
            WikidataServiceResponseBody.fromError("Q$qid", "Q$qid", "", "Could not connect to service.")
        } catch (e: CertPathBuilderException) {
            log.error("Certificate validation failed.")
            WikidataServiceResponseBody.fromError("Q$qid", "Q$qid", "", "Certificate validation failed.")
        } catch (e: Exception) {
            log.error("An error occurred while requesting the wikidata linker ${e.message}.")
            WikidataServiceResponseBody.fromError(
                "Q$qid",
                "Q$qid",
                "",
                "An unexpected problem occurred while requesting the Wikidata service: ${e.message}."
            )
        }
    }

    @OptIn(ExperimentalSerializationApi::class)
    fun requestConnectomData(memobaseId: String): ConnectomServiceResponse {
        val httpGet = HttpRequest.newBuilder()
            .uri(URI.create("$connectomServiceUrl/$memobaseId"))
            .header("Accept", "application/json")
            .header("Accept-Charset", "utf-8")
            .GET()
            .build()

        return try {
            val response = graphQlServiceClient.send(httpGet, java.net.http.HttpResponse.BodyHandlers.ofInputStream())
            when (response.statusCode()) {
                200 -> {
                    try {
                        return json.decodeFromStream(response.body())
                    } catch (ex: SerializationException) {
                        log.error(
                            "Error processing json content: " + ex.message + response.body().readAllBytes()
                                .toString(charset = Charsets.UTF_8)
                        )
                        return ConnectomServiceResponse.fromError(
                            "An error occurred while processing the response from the Connectom service."
                        )
                    }
                }

                404 -> {
                    return ConnectomServiceResponse.fromError("No matches found for value $memobaseId.")
                }

                else -> {
                    log.error("An error occurred while requesting the Connectom service with code ${response.statusCode()} for $memobaseId.")
                    return ConnectomServiceResponse.fromError(
                        "An error occurred while requesting the Connectom service with code ${response.statusCode()} for $memobaseId."
                    )
                }
            }
        } catch (e: Exception) {
            log.error("An error occurred while requesting the Connectom service ${e.message}.")
            ConnectomServiceResponse.fromError(
                "An unexpected problem occurred while requesting the connectom service: ${e.message}."
            )
        }
    }
}

/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.helpers

import nl.altindag.ssl.SSLFactory
import nl.altindag.ssl.pem.util.PemUtils
import java.nio.file.Paths
import javax.net.ssl.SSLContext

/**
 * Generates an SSL Context based on the provided trust material.
 *
 * @param certificateAuthorityPath Path of the certificate authority file.
 * @param clientCertificatePath Path of the client certificate file.
 * @param clientKeyPath Path of the client key file.
 */
internal fun getSSLContext(
    certificateAuthorityPath: String,
    clientCertificatePath: String,
   clientKeyPath: String,
): SSLContext {
    return SSLFactory.builder()
        .withIdentityMaterial(PemUtils.loadIdentityMaterial(Paths.get(clientCertificatePath), Paths.get(clientKeyPath)))
        .withTrustMaterial(PemUtils.loadTrustMaterial(Paths.get(certificateAuthorityPath)))
        .withDefaultTrustMaterial()
        .withSystemTrustMaterial()
        .build().sslContext
}
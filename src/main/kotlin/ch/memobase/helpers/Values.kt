/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.helpers

object Values {
    const val LANGUAGE_NORMALIZER_MECHANISM_NAME = "LanguagesNormalizer"
    const val GENRE_NORMALIZER_MECHANISM_NAME = "GenreNormalizer"
    const val CARRIER_TYPE_NORMALIZER_MECHANISM_NAME = "CarrierTypeNormalizer"
    const val FIRST_TO_LAST_NAME = "first-to-last"
    const val LAST_TO_FIRST_NAME = "last-to-first"

    const val EXTRACT_RELATION_NAME_GROUP_PATTERN_NAME = "relation"

    const val MECHANISM_NAME_ENRICHED_BY_CONNECTOM = "Enriched by Connectom"
    const val MECHANISM_NAME_WIKIDATA_LINKER_PLACE= "Wikidata Linker - Place"
    const val MECHANISM_NAME_WIKIDATA_LINKER_AGENT = "Wikidata Linker - Agent"

    const val ENTITY_RICO_TYPE_ENRICHED_BY_CONNECTOM = "Enriched by Connectom"
    const val ENTITY_RICO_TYPE_ENRICHED_BY_MEMOBASE = "Enriched by Memobase"

    const val MISSING_LABEL_DE = "FEHLENDES LABEL"
    const val MISSING_LABEL_FR = "L'ÉTIQUETTE MANQUANTE"
    const val MISSING_LABEL_IT = "GALATEO MANCANTE"

    const val KS_CONFIG_NAME = "check-config"
    const val KS_CONFIG_VALUE = "value"
    const val KS_CONFIG_EXCEPTION = "exception"

    const val KS_TRANSFORM_NAME = "parse-transform"
    const val KS_TRANSFORM_FATAL = "fatal"
    const val KS_TRANSFORM_SUCCESS = "success"

    const val KS_MODEL_NAME = "parse-model"
    const val KS_MODEL_FATAL = "fatal"
    const val KS_MODEL_SUCCESS = "success"
}

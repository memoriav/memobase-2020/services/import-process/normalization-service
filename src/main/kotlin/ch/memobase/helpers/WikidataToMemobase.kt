package ch.memobase.helpers

import ch.memobase.rdf.*
import ch.memobase.schema.external.WikidataServiceResponseBody
import ch.memobase.transform.WikidataLinksEnricher
import org.apache.jena.rdf.model.Resource
import org.slf4j.LoggerFactory

object WikidataToMemobase {
    private val log = LoggerFactory.getLogger(WikidataToMemobase::class.java)

    fun transformResponse(
        model: MemobaseModel,
        rdfType: Resource,
        ricoType: String,
        metadata: WikidataServiceResponseBody,
        issues: MutableList<String>,
        qid: Int,
    ): RicoResource? {
        val newResource = model.createRicoResource(rdfType)
            .addLiteral(RICO.type, ricoType)

        val instanceOf = metadata.data?.get(WD.INSTANCE_OF_P31)
        if (instanceOf != null) {
            for (instance in instanceOf) {
                instance.simple?.let { instanceOfId ->
                    if (instanceOfId == "Q4167410") {
                        log.error("Found match to Wikimedia disambiguation page (Q$qid). This match was $ricoType.")
                        issues.add("Found match to Wikimedia disambiguation page (Q$qid). This match was $ricoType.")
                        return null
                    } else {
                        newResource.addLiteral(WD.instanceOf, instanceOfId)
                    }
                }
            }
        }

        newResource.addLiteral(SCHEMA.sameAs, "${WikidataLinksEnricher.WIKIDATA_BASE_URL}/Q${qid}")

        addLabels(newResource, metadata)
        addDescriptions(newResource, metadata)

        val gndIds = metadata.data?.get(WD.GND_ID_P227)
        if (gndIds != null) {
            for (gnd in gndIds) {
                newResource.addLiteral(SCHEMA.sameAs, "${WikidataLinksEnricher.GND_BASE_URL}/${gnd.simple}")
            }
        }

        val hlsIds = metadata.data?.get(WD.HDS_ID_P902)
        if (hlsIds != null) {
            for (hls in hlsIds) {
                newResource.addLiteral(SCHEMA.sameAs, "${WikidataLinksEnricher.HLS_BASE_URL}/${hls.simple}")
            }
        }

        val coordinates = metadata.data?.get(WD.COORDINATE_LOCATION_P625)
        if (coordinates != null) {
            for (coordinate in coordinates) {
                coordinate.globeCoordinate?.let {
                    if (it.latitude == null || it.longitude == null) {
                        issues.add("Could not find coordinates for the following qid: $qid.")
                    } else {

                        newResource.addTypedLiteral(
                            WD.coordinateLocation,
                            createPoint(it.latitude, it.longitude),
                            GEO.wktLiteral
                        )
                    }
                }

            }
        }

        val coordinatesOfGeographicCenter =
            metadata.data?.get(WD.COORDINATES_OF_GEOGRAPHIC_CENTER_P5140)
        if (coordinatesOfGeographicCenter != null) {
            for (coordinate in coordinatesOfGeographicCenter) {
                coordinate.globeCoordinate?.let {
                    if (it.latitude == null || it.longitude == null) {
                        issues.add("Could not find coordinate of geographic center for the following qid: $qid.")
                    } else {
                        newResource.addTypedLiteral(
                            WD.coordinatesOfGeographicCenter,
                            createPoint(it.latitude, it.longitude),
                            GEO.wktLiteral
                        )
                    }
                }
            }
        }
        return newResource
    }


    private fun addLabels(resource: RicoResource, metadata: WikidataServiceResponseBody) {
        fun addLabelIfNotNull(label: String?, language: String) {
            label?.let {
                resource.addLiteral(RICO.name, it, language)
            }
        }

        metadata.labels?.let { labels ->
            addLabelIfNotNull(labels.de, "de")
            addLabelIfNotNull(labels.fr, "fr")
            addLabelIfNotNull(labels.it, "it")
        }
    }

    private fun addDescriptions(resource: RicoResource, metadata: WikidataServiceResponseBody) {
        fun addDescriptionIfNotNull(description: String?, language: String) {
            description?.let {
                resource.addLiteral(RICO.descriptiveNote, it, language)
            }
        }

        metadata.descriptions?.let { descriptions ->
            addDescriptionIfNotNull(descriptions.de, "de")
            addDescriptionIfNotNull(descriptions.fr, "fr")
            addDescriptionIfNotNull(descriptions.it, "it")
        }
    }

    private fun createPoint(latitude: String, longitude: String): String {
        return "Point($latitude $longitude)"
    }
}
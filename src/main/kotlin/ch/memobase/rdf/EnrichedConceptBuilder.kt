/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.rdf

import ch.memobase.helpers.Values
import ch.memobase.schema.Labels
import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource
import org.apache.jena.rdf.model.Statement

class EnrichedConceptBuilder(
    private val rdfType: Resource,
    private val model: MemobaseModel,
    private val target: RicoResource,
    private val targetProperty: Property
) {

    private var resource: RicoResource? = null

    fun init(): EnrichedConceptBuilder {
        resource = model.createRicoResource(rdfType)
        return this
    }

    fun addLabels(property: Property, labels: Labels): EnrichedConceptBuilder {
        resource
            ?.addLiteral(property, labels.de.ifEmpty { Values.MISSING_LABEL_DE }, "de")
            ?.addLiteral(property, labels.fr.ifEmpty { Values.MISSING_LABEL_FR }, "fr")
            ?.addLiteral(property, labels.it.ifEmpty { Values.MISSING_LABEL_IT }, "it")
        return this
    }

    fun addSameAsLink(facet: String): EnrichedConceptBuilder {
        resource?.addLiteral(SCHEMA.sameAs, NS.wd + facet)
        return this
    }

    fun appendExistingActivity(activity: Resource): EnrichedConceptBuilder {
        resource?.let {
            it.addProperty(RICO.resultsOrResultedFrom, activity)
            activity.addProperty(RICO.resultsOrResultedIn, it.resource)
        }
        return this
    }

    fun addOtherStatements(statements: Iterable<Statement>): EnrichedConceptBuilder {
        statements.forEach { resource?.addProperty(it.predicate, it.`object`) }
        return this
    }

    fun build() {
        resource?.let { target.addProperty(targetProperty, it) }
    }
}
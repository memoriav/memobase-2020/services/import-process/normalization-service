/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.rdf

import ch.memobase.exceptions.InvalidInputException
import org.apache.jena.graph.GraphMemFactory
import org.apache.jena.query.*
import org.apache.jena.rdf.model.Resource
import org.apache.jena.rdf.model.impl.ModelCom
import org.apache.jena.riot.RDFDataMgr
import org.apache.jena.riot.RDFFormat
import java.io.StringWriter


class MemobaseModel : ModelCom(GraphMemFactory.createGraphMem()) {

    fun createRicoResource(rdfType: Resource, ricoType: String): RicoResource {
        return RicoResource(this.createResource(), rdfType, ricoType)
    }

    fun createRicoResource(rdfType: Resource): RicoResource {
        return RicoResource(this.createResource(), rdfType)
    }


    private val querySource = """
        PREFIX rico: <https://www.ica.org/standards/RiC/ontology#>

        SELECT ?id
        {

            ?record a rico:Record .
            ?record rico:hasOrHadIdentifier ?identifier .
            ?identifier rico:type "main" .
            ?identifier rico:identifier ?id .
        }
    """.trimIndent()

    fun getRecordId(): String {
        val query: Query = QueryFactory.create(querySource)

        QueryExecutionFactory.create(query, this).use { qexec ->
            val results: ResultSet = qexec.execSelect()
            while (results.hasNext()) {
                val solution = results.nextSolution()
                val x = solution["id"]
                if (x != null) {
                    return x.asLiteral().string
                }
            }
        }
        throw InvalidInputException("Source did not contain a main record id.")
    }

    fun listRicoResourceSubjects(): Iterable<RicoResource> {
        return this.listSubjects().mapWith {
            try {
                RicoResource(it)
            } catch (ex: InvalidInputException) {
                null
            }
        }.toList().filterNotNull()
    }

    override fun toString(): String {
        val out = StringWriter()
        RDFDataMgr.write(out, this, RDFFormat.TURTLE_PRETTY)
        return out.toString()
    }

    fun getRecord(): RicoResource {
        try {
            return listRicoResourceSubjects().first {
                it.rdfType == RICO.Record
            }
        } catch (ex: NoSuchElementException) {
            throw InvalidInputException("No record found in model")
        }
    }
}

/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.rdf

import ch.memobase.exceptions.InvalidInputException
import org.apache.jena.rdf.model.*
import org.apache.jena.riot.RDFDataMgr
import org.apache.jena.riot.RDFFormat
import org.apache.logging.log4j.LogManager
import java.io.StringWriter

class RicoResource(val resource: Resource) {
    constructor(resource: Resource, rdfType: Resource) : this(resource.addProperty(RDF.type, rdfType))
    constructor(resource: Resource, rdfType: Resource, ricoType: String) : this(
        resource.addProperty(RDF.type, rdfType).addProperty(RICO.type, ricoType)
    )

    private val log = LogManager.getLogger("RicoResource")

    var rdfType = retrieveRdfType(resource)
    private val ricoType: String? = resource.getProperty(RICO.type)?.string

    private fun retrieveRdfType(resource: Resource): Resource {
        return try {
            resource.getProperty(RDF.type).resource
        } catch (ex: IllegalStateException) {
            val id = if (resource.isAnon) {
                resource.id.labelString
            } else {
                resource.uri
            }
            val message = "Resource without a rdf:type: $id" + ex.localizedMessage
            log.error(message)
            throw InvalidInputException(message)
        }
    }

    private fun getLabel(): String {
        return when (rdfType) {
            RICO.Identifier -> getStringLiteral(RICO.identifier).orEmpty()
            RICO.Record, RICO.Title -> getStringLiteral(RICO.title).orEmpty()
            RICO.DateRange, RICO.DateSet, RICO.SingleDate ->
                getStringLiteral(RICO.normalizedDateValue) ?: getStringLiteral(RICO.expressedDate).orEmpty()
            else -> getStringLiteral(RICO.name) ?: ""
        }
    }

    fun hasType(type: Resource): Boolean {
        return rdfType == type
    }

    fun getType(): Resource {
        return rdfType
    }

    fun hasProperty(property: Property): Boolean {
        return resource.hasProperty(property)
    }

    fun hasProperty(property: Property, value: String): Boolean {
        return resource.hasProperty(property, value)
    }

    fun hasProperty(property: Property, value: Resource): Boolean {
        return resource.hasProperty(property, value)
    }

    fun hasProperty(property: Property, value: RicoResource): Boolean {
        return resource.hasProperty(property, value.resource)
    }

    fun hasLiteral(property: Property, value: String): Boolean {
        return resource.hasLiteral(property, value)
    }

    fun listProperties(property: Property): Iterable<Statement> {
        return resource.listProperties(property).toList()
    }

    fun listProperties(): Iterable<Statement> {
        return resource.listProperties().toList()
    }

    fun getStringLiteral(property: Property): String? {
        return resource.getProperty(property)?.string
    }

    fun getLiteralLanguage(property: Property): String {
        return resource.getProperty(property).language
    }

    fun getResource(property: Property): RicoResource {
        return RicoResource(resource.getPropertyResourceValue(property))
    }

    fun addProperty(property: Property, objectResource: Resource): RicoResource {
        resource.addProperty(property, objectResource)
        return this
    }

    fun addProperty(property: Property, objectResource: RicoResource): RicoResource {
        resource.addProperty(property, objectResource.resource)
        return this
    }


    fun addProperty(predicate: Property, rdfNode: RDFNode): RicoResource {
        resource.addProperty(predicate, rdfNode)
        return this
    }

    fun addLiteral(property: Property, value: String): RicoResource {
        resource.addProperty(property, value)
        return this
    }

    fun addLiteral(property: Property, value: String, lang: String): RicoResource {
        resource.addProperty(property, value, lang)
        return this
    }

    fun addTypedLiteral(property: Property, value: String, dataType: Property): RicoResource {
        resource.addLiteral(property, resource.model.createTypedLiteral(value, dataType.uri))
        return this
    }

    fun removeAllProperties(property: Property): RicoResource {
        resource.removeAll(property)
        return this
    }

    fun removeStatement(statement: Statement): RicoResource {
        resource.model.remove(statement)
        return this
    }

    fun removeAllProperties(property: Property, value: String): RicoResource {
        resource.model.remove(resource.listProperties(property).filterKeep { it.`object`.isLiteral }
            .filterKeep { it.string == value }.toList())
        return this
    }

    fun replaceRdfType(new: Resource): RicoResource {
        resource.removeAll(RDF.type)
        resource.addProperty(RDF.type, new)
        rdfType = new
        return this
    }

    override fun toString(): String {
        val out = StringWriter()
        val model = ModelFactory.createDefaultModel()
        model.add(resource.listProperties())
        RDFDataMgr.write(out, model, RDFFormat.TURTLE_PRETTY)
        return out.toString()
    }
}

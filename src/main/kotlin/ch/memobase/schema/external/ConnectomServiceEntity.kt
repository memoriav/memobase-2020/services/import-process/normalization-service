package ch.memobase.schema.external

import kotlinx.serialization.Serializable

@Serializable
data class ConnectomServiceEntity(
    val sameAs: List<String>,
    val text: String,
    val start: Int,
    val end: Int
)

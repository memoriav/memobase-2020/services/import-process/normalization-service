package ch.memobase.schema.external

import kotlinx.serialization.Serializable

@Serializable
data class ConnectomServiceResponse(
    val id: String,
    val spatials: List<ConnectomServiceEntity>,
    val agents: List<ConnectomServiceEntity>,
    val success: Boolean = true,
) {
    companion object {

        fun fromError(error: String): ConnectomServiceResponse {
            return ConnectomServiceResponse(
                id = error,
                spatials = emptyList(),
                agents = emptyList(),
                success = false,
            )
        }
    }
}

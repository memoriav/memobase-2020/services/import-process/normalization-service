package ch.memobase.schema.external

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class WikidataLinkerRequestBody(
    val value: String,
    @SerialName("value_type")
    val valueType: WikidataLinkerValueType,
    val collection: String,
    val verbose: Boolean,
)

@Serializable
enum class WikidataLinkerValueType {
    @SerialName("location")
    LOCATION,

    @SerialName("agent")
    AGENT,
}

@Serializable
data class WikidataLinkerResponseBody(
    val value: String,
    val matches: List<Match>,
    val extractor: String,
)

@Serializable
data class Match(
    val value: String,
    val qid: Int,
    @SerialName("match_type")
    val matchType: String,
    val range: Range,
    val lang: List<String>,
)

@Serializable
data class Range(
    val start: Int,
    val end: Int,
)

@Serializable
data class WikidataServiceRequestBody(
    val includeSiteLinks: Boolean,
    val languages: List<String>,
    val properties: List<String>,
)

@Serializable
data class WikidataLabels(
    val de: String? = null,
    val fr: String? = null,
    val it: String? = null,
)

@Serializable
data class WikidataServiceResponseBody(
    val id: String,
    val originId: String,
    val lastModified: String,
    val labels: WikidataLabels? = null,
    val descriptions: WikidataLabels? = null,
    val data: Map<String, List<WikidataPropertyData>>? = null,
    val error: String? = null,
) {
    companion object {
        fun fromError(id: String, originId: String, lastModified: String, error: String): WikidataServiceResponseBody {
            return WikidataServiceResponseBody(
                id = id,
                originId = originId,
                lastModified = lastModified,
                labels = WikidataLabels("", "", ""),
                descriptions = WikidataLabels("", "", ""),
                data = mapOf(),
                error = error,
            )
        }
    }
}

@Serializable
data class WikidataPropertyData(
    val globeCoordinate: GlobeCoordinate? = null,
    val monoLingualText: MonoLingualText? = null,
    val quantity: Quantity? = null,
    val simple: String? = null,
    val time: String? = null,
)

@Serializable
data class GlobeCoordinate(
    val globe: String,
    val latitude: String? = null,
    val longitude: String? = null,
    val precision: Double? = null,
)

@Serializable
data class MonoLingualText(
    val language: String,
    val value: String,
)

@Serializable
data class Quantity(
    val amount: String,
    val unit: String,
    val lowerbound: String,
    val upperbound: String,
)
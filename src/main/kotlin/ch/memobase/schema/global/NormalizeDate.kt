/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schema.global

import ch.memobase.exceptions.InvalidMappingException
import ch.memobase.helpers.ValidationError
import ch.memobase.transform.DateNormalizationTransform
import kotlinx.serialization.Serializable
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths
import java.util.regex.PatternSyntaxException

@Serializable
data class NormalizeDate(
    val qualifiers: String,
    val certainties: String,
    val singeDateMatchers: String,
    val dateRangeMatchers: String
) {

    fun generate(): DateNormalizationTransform {
        val qualifierRegexList = validate(
            qualifiers, "Qualifier",
            { index: Int, value: String ->
                if (!value.contains(Regex("\\(\\?<qualifier>.*\\)"))) {
                    throw ValidationError("[Date:Qualifier] Regex does not contain group 'qualifier' (Line: ${index + 1}).")
                }
            }, true
        )
        val certaintyRegexList = validate(
            certainties, "Certainty",
            { index: Int, value: String ->
                if (!value.contains(Regex("\\(\\?<certainty>.*\\)"))) {
                    throw ValidationError("[Date:Certainty] Regex does not contain group 'certainty' (Line: ${index + 1}).")
                }
            }, true
        )
        val singleDateRegexList = validate(
            singeDateMatchers, "SingleDate",
            { index: Int, value: String ->
                if (!value.contains(Regex("\\(\\?<day>.*\\)"))) {
                    throw ValidationError("[Date:SingleDate] Regex does not contain group 'day' (Line: ${index + 1}).")
                }
                if (!value.contains(Regex("\\(\\?<month>.*\\)"))) {
                    throw ValidationError("[Date:SingleDate] Regex does not contain group 'month' (Line: ${index + 1}).")
                }
                if (!value.contains(Regex("\\(\\?<year>.*\\)"))) {
                    throw ValidationError("[Date:SingleDate] Regex does not contain group 'year' (Line: ${index + 1}).")
                }
            }, false
        )
        val dateRangeRegexList = validate(
            dateRangeMatchers, "DateRange",
            { index: Int, value: String ->
                if (!value.contains(Regex("\\(\\?<singleYear>.*\\)")) &&
                    !(value.contains(Regex("\\(\\?<fromYear>.*\\)")) &&
                        value.contains(Regex("\\(\\?<untilYear>.*\\)")))
                ) {
                    throw ValidationError(
                        "[Date:DateRange] Regex does not contain group 'singleYear' or " +
                            "'fromYear' and 'untilYear' (Line: ${index + 1})."
                    )
                }
            }, false
        )
        return DateNormalizationTransform(
            qualifierValues = qualifierRegexList,
            certaintyValues = certaintyRegexList,
            singleDateMatchers = singleDateRegexList,
            dateRangeMatchers = dateRangeRegexList
        )
    }

    private fun validate(
        path: String,
        type: String,
        check: (Int, String) -> Unit,
        option: Boolean
    ): List<Regex> {
        return getLines(path).mapIndexed { index, value ->
            check(index, value)
            try {
                if (option)
                    Regex(value, RegexOption.IGNORE_CASE)
                else
                    Regex(value)
            } catch (ex: PatternSyntaxException) {
                throw ValidationError(
                    "[Date:${type}] Invalid Regex Pattern: ${
                        ex.localizedMessage.replace(
                            "\n",
                            " "
                        )
                    } (Line: ${index + 1})."
                )
            }
        }
    }

    private fun getLines(path: String): List<String> {
        try {
            return Files.newBufferedReader(Paths.get(path)).readLines()
        } catch (ex: IOException) {
            throw InvalidMappingException("File Not Found: ${ex.localizedMessage}.")
        }
    }
}

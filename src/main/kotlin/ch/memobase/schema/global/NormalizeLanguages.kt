/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schema.global

import ch.memobase.helpers.LoadFile
import ch.memobase.helpers.ValidationError
import ch.memobase.transform.LanguagesNormalizer
import com.github.doyaaaaaken.kotlincsv.util.CSVFieldNumDifferentException
import com.github.doyaaaaaken.kotlincsv.util.CSVParseFormatException
import kotlinx.serialization.Serializable
import java.io.FileNotFoundException
import java.io.IOException

@Serializable
data class NormalizeLanguages(
    val facets: String,
    val labels: String
) {
    fun generate(): LanguagesNormalizer {
        try {
            return LanguagesNormalizer(LoadFile.readFacetList(facets), LoadFile.readLabelFile(labels))
        } catch (e: FileNotFoundException) {
            throw ValidationError("[Language] File not found at $facets or $labels. ${e.message}")
        } catch (e: IOException) {
            throw ValidationError("[Language] IO Exception: ${e.message}")
        } catch (ex: CSVParseFormatException) {
            throw ValidationError("[Language] CSV Exception: ${ex.localizedMessage}.")
        } catch (ex: CSVFieldNumDifferentException) {
            throw ValidationError("[Language] CSV Field Num Exception: ${ex.localizedMessage}.")
        } catch (ex: ValidationError) {
            throw ValidationError("[Language] OutOfBoundsException ${ex.localizedMessage}.")
        } catch (ex: Exception) {
            throw ValidationError("[Language] Exception: ${ex.localizedMessage}.")
        }
    }
}

/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schema.local

import ch.memobase.helpers.ValidationError
import ch.memobase.rdf.NS
import ch.memobase.transform.LanguageTagEnricher
import kotlinx.serialization.Serializable
import org.apache.jena.rdf.model.ResourceFactory

@Serializable
data class EnrichLanguageTags(
    val language: String,
    val include: Map<String, List<String>>? = null,
    val exclude: Map<String, List<String>>? = null,
) {

    private val defaultPropertiesList: Map<String, List<String>> = mapOf(
        "rico:Record" to listOf(
            "rico:title",
            "dcterms:abstract",
            "dcterms:relation",
            "rico:descriptiveNote",
            "rico:scopeAndContent",
            "rico:source",
            "rico:conditionsOfAccess",
            "rico:conditionsOfUse"
        ),
        "rico:Title" to listOf("rico:title"),
        "rico:Agent" to listOf("rico:descriptiveNote"),
        "rico:CorporateBody" to listOf("rico:descriptiveNote"),
        "rico:Person" to listOf(
            "rico:descriptiveNote", "rdau:P60468", // profession or occupation
            "foaf:gender"
        ),
        "rico:CreationRelation" to listOf("rico:name"),
        "rico:Language" to listOf("rico:name"),
        "rico:CarrierType" to listOf("rico:name"),
        "skos:Concept" to listOf("skos:prefLabel"),
        "rico:Instantiation" to listOf(
            "rico:descriptiveNote",
            "rico:physicalCharacteristics",
            "rdau:P60558", // colour component
        )
    )

    fun generate(): LanguageTagEnricher {
        if (language !in listOf("de", "fr", "it")) {
            throw ValidationError("Property 'language' must be one of 'de', 'fr', 'it', instead was '$language'.")
        }

        val properties = mutableMapOf<String, List<String>>()

        defaultPropertiesList.forEach { (key, value) ->
            val inclusiveValue = include.let {
                if (it != null) {
                    if (it.containsKey(key)) {
                        value + it[key]!!
                    } else {
                        value
                    }
                } else {
                    value
                }
            }

            exclude.let { excludedList ->
                if (excludedList != null) {
                    if (excludedList.containsKey(key)) {
                        properties[key] = inclusiveValue.filter { !excludedList[key]!!.contains(it) }
                    } else {
                        properties[key] = inclusiveValue
                    }
                } else {
                    properties[key] = inclusiveValue
                }
            }
        }

        include?.forEach {
            if (!properties.containsKey(it.key)) {
                properties[it.key] = it.value
            }
        }

        exclude?.forEach {
            if (!properties.containsKey(it.key)) {
                throw ValidationError(
                    "Tried to exclude resource '${it.key}' from default list, but it is not present. " +
                            "Exclude can only remove properties from resources that are in the default list. ${defaultPropertiesList.keys}}"
                )
            }
        }

        val resourceMapping = properties.map { entry ->
            entry.key.split(":").let { values ->
                if (values.size != 2) throw ValidationError(
                    "Mapping key is missing a prefix to map to a resource. Correct syntax is 'prefix:ClassName'. Found '${entry.key}' instead."
                )
                val mappedPrefix = NS.prefixMapping[values[0]] ?: throw ValidationError(
                    "Prefix does not exist. Should be one of ${NS.prefixMapping.keys}. Found '${values[0]}' instead fir ${entry.key}."
                )
                ResourceFactory.createResource(mappedPrefix + values[1])
            } to entry.value.map { property ->
                property.split(":").let { values ->
                    if (values.size != 2) throw ValidationError(
                        "Mapped property is missing a prefix to map to a resource. Correct syntax is 'prefix:propertyName'. Found '${property}' instead."
                    )
                    val mappedPrefix = NS.prefixMapping[values[0]] ?: throw ValidationError(
                        "Prefix does not exist. Should be one of ${NS.prefixMapping.keys}. Found '${values[0]}' instead for ${property}."
                    )
                    ResourceFactory.createProperty(mappedPrefix, values[1])
                }
            }
        }.toMap()

        return LanguageTagEnricher(
            language = language, properties = resourceMapping
        )
    }

}
package ch.memobase.schema.local

import ch.memobase.helpers.RequestClient
import ch.memobase.transform.WikidataLinksEnricher
import kotlinx.serialization.Serializable

@Serializable
data class EnrichWikidata(
    val recordSetId: String,
) {
    fun generate(
        requestClient: RequestClient,
    ): WikidataLinksEnricher {
        return WikidataLinksEnricher(
            client = requestClient,
            recordSetId = recordSetId
        )
    }
}

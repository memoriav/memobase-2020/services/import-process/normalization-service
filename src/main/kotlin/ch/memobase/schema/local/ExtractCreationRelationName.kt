/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schema.local

import ch.memobase.helpers.Values.EXTRACT_RELATION_NAME_GROUP_PATTERN_NAME
import ch.memobase.helpers.ValidationError
import ch.memobase.transform.ExtractCreationRelationNameTransform
import kotlinx.serialization.Serializable

@Serializable
data class ExtractCreationRelationName(
    val pattern: String,
    val language: String
) {
    private val validLanguageTags = listOf(
        "de", "fr", "it", "NONE"
    )

    fun generate(): ExtractCreationRelationNameTransform {
        if (!pattern.contains("(?<$EXTRACT_RELATION_NAME_GROUP_PATTERN_NAME>")) {
            throw ValidationError("Property 'extractRelationName.pattern' requires a named group '(?<$EXTRACT_RELATION_NAME_GROUP_PATTERN_NAME>[captured-text])' to be defined, instead was '$pattern'.")
        }
        if (!validLanguageTags.contains(language)) {
            throw ValidationError(
                "Property 'extractRelationName.language' must be one of ${
                    validLanguageTags.joinToString(
                        ", "
                    )
                }, instead was '$language'."
            )
        }
        return ExtractCreationRelationNameTransform(Regex(pattern), if (language == "NONE") "" else language)
    }
}

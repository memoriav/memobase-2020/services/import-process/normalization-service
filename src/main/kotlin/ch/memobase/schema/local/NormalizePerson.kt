/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schema.local

import ch.memobase.helpers.ValidationError
import ch.memobase.helpers.Values.FIRST_TO_LAST_NAME
import ch.memobase.helpers.Values.LAST_TO_FIRST_NAME
import ch.memobase.transform.ITransformer
import ch.memobase.transform.PersonNormalizer
import kotlinx.serialization.Serializable
import org.apache.logging.log4j.LogManager

@Serializable
data class NormalizePerson(
    val splitEntity: SplitEntity? = null,
    val creationRelationName: ExtractCreationRelationName? = null,
    val nameOrder: String,
    val singleNameIsLastName: Boolean,
    val nameDelimiter: String
) {
    private val log = LogManager.getLogger(this::class.java)

    private val validNameOrders = listOf(
        FIRST_TO_LAST_NAME,
        LAST_TO_FIRST_NAME
    )

    fun generate(): List<ITransformer> {
        if (!validNameOrders.contains(nameOrder))
            throw ValidationError("Property 'nameOrder' requires one of ${validNameOrders.joinToString(", ")}, instead was '$nameOrder'.")
        val delimiter = if (nameDelimiter == "SPACE") " " else nameDelimiter
        return listOfNotNull(
            splitEntity?.generate(),
            creationRelationName?.generate(),
            PersonNormalizer(nameOrder, singleNameIsLastName, delimiter)
        )
    }
}

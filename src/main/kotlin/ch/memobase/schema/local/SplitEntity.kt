/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schema.local

import ch.memobase.exceptions.InvalidMappingException
import ch.memobase.helpers.ValidationError
import ch.memobase.rdf.NS
import ch.memobase.transform.SplitEntityTransform
import kotlinx.serialization.Serializable
import org.apache.jena.rdf.model.ResourceFactory

@Serializable
data class SplitEntity(
    val type: String,
    val property: String,
    val delimiter: String
) {
    fun generate(): SplitEntityTransform {
        if (!type.contains(":")) {
            throw ValidationError("Property 'type' should have format 'prefix:ClassName', but was '$type' instead.")
        }
        if (!property.contains(":")) {
            throw ValidationError("Property 'property' should have format 'prefix:propertyName', but was '$property' instead.")
        }
        val typeParts = type.split(":")
        val propertyParts = property.split(":")
        val typeNamespace = try {
            NS.prefixToNamespace(typeParts[0])
        } catch (ex: InvalidMappingException) {
            throw ValidationError(ex.localizedMessage)
        }
        val propertyNamespace = try {
            NS.prefixToNamespace(propertyParts[0])
        } catch (ex: InvalidMappingException) {
            throw ValidationError(ex.localizedMessage)
        }
        return SplitEntityTransform(
            type = ResourceFactory.createResource(typeNamespace + typeParts[1]),
            literal = ResourceFactory.createProperty(propertyNamespace, propertyParts[1]),
            delimiter = if (delimiter == "SPACE") " " else delimiter
        )
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SplitEntity

        if (type != other.type) return false
        if (property != other.property) return false
        if (delimiter != other.delimiter) return false

        return true
    }
}

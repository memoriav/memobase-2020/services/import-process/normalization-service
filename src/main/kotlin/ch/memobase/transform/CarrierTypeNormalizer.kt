/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.transform

import ch.memobase.helpers.Activity
import ch.memobase.helpers.Values
import ch.memobase.schema.Facets
import ch.memobase.schema.Labels
import ch.memobase.rdf.EnrichedConceptBuilder
import ch.memobase.rdf.MemobaseModel
import ch.memobase.rdf.RDF
import ch.memobase.rdf.RICO
import ch.memobase.rdf.RicoResource
import org.apache.logging.log4j.LogManager
import java.util.*

class CarrierTypeNormalizer(
    private val facetsMap: Map<String, Facets>,
    private val labelsMap: Map<String, Labels>
) : ITransformer {
    private val log = LogManager.getLogger(this::class.java)

    override fun transform(item: RicoResource, record: RicoResource, model: MemobaseModel): List<String> {
        return if (item.hasType(RICO.CarrierType)) {
            if (item.hasProperty(RICO.name)) {
                val allStatements = item.listProperties()
                val copyStatements =
                    allStatements.filter { it.predicate != RICO.name }.filter { it.predicate != RDF.type }
                item.listProperties(RICO.name).mapNotNull { statement ->
                    val nameValue = statement.string.trim()
                    val lowerCaseValue = nameValue.lowercase(Locale.getDefault())
                    val physicalInstantiation = findPhysicalInstantiation(model)
                    val builder = EnrichedConceptBuilder(RICO.CarrierType, model, physicalInstantiation, RICO.hasCarrierType)
                    facetsMap[lowerCaseValue].let { facetValues ->
                        if (facetValues != null) {
                            val activity = Activity.createActivity(Values.CARRIER_TYPE_NORMALIZER_MECHANISM_NAME, model)
                            activity.addProperty(RICO.affectsOrAffected, item.resource)
                            item.resource.addProperty(RICO.isOrWasAffectedBy, activity)

                            facetValues.wikidata.forEach { facetValue ->
                                builder
                                    .init()
                                    .addLabels(RICO.name, labelsMap.getOrDefault(facetValue, Labels.default))
                                    .addSameAsLink(facetValue)
                                    .addOtherStatements(copyStatements)
                                    .appendExistingActivity(activity)
                                    .build()
                            }
                            facetValues.freeFacetValue.forEach { facetValue ->
                                builder
                                    .init()
                                    .addLabels(RICO.name, labelsMap.getOrDefault(facetValue, Labels.default))
                                    .addOtherStatements(copyStatements)
                                    .appendExistingActivity(activity)
                                    .build()
                            }
                            null
                        } else {
                            log.error("Could not map the following carrier type value: $nameValue.")
                            "Could not map the following carrier type value: $nameValue."
                        }
                    }

                }
            } else {
                log.error("Carrier type without a name found: $item}.")
                listOf("Carrier type without a name found: $item.")
            }
        } else {
            emptyList()
        }
    }

    private fun findPhysicalInstantiation(model: MemobaseModel): RicoResource {
        return model.listRicoResourceSubjects().filter { it.hasProperty(RDF.type, RICO.Instantiation) }
            .first { it.hasLiteral(RICO.type, RICO.Types.Instantiation.physicalObject) }
    }
}

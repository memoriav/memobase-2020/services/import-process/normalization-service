/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.transform

import ch.memobase.exceptions.InvalidInputException
import ch.memobase.helpers.Date
import ch.memobase.helpers.Date.CERTAINTY_GROUP
import ch.memobase.helpers.Date.DAY_FROM_GROUP
import ch.memobase.helpers.Date.DAY_GROUP
import ch.memobase.helpers.Date.DAY_UNTIL_GROUP
import ch.memobase.helpers.Date.MONTH_FROM_GROUP
import ch.memobase.helpers.Date.MONTH_GROUP
import ch.memobase.helpers.Date.MONTH_UNTIL_GROUP
import ch.memobase.helpers.Date.QUALIFIER_GROUP
import ch.memobase.helpers.Date.SINGLE_MONTH_GROUP
import ch.memobase.helpers.Date.SINGLE_YEAR_GROUP
import ch.memobase.helpers.Date.YEAR_FROM_GROUP
import ch.memobase.helpers.Date.YEAR_GROUP
import ch.memobase.helpers.Date.YEAR_UNTIL_GROUP
import ch.memobase.rdf.MemobaseModel
import ch.memobase.rdf.RDF
import ch.memobase.rdf.RICO
import ch.memobase.rdf.RicoResource
import org.apache.logging.log4j.LogManager

class DateNormalizationTransform(
    private val singleDateMatchers: List<Regex>,
    private val dateRangeMatchers: List<Regex>,
    private val certaintyValues: List<Regex>,
    private val qualifierValues: List<Regex>
) : ITransformer {
    override fun transform(item: RicoResource, record: RicoResource, model: MemobaseModel): List<String> {
        if (!item.hasType(RICO.DateSet))
            return emptyList()

        val originalDateValue = item.getStringLiteral(RICO.expressedDate)
        if (originalDateValue.isNullOrBlank()) {
            throw InvalidInputException("No or blank date value found for rico:DateSet.")
        }
        val trimmedValue = originalDateValue.trim()


        val qualifiers = extractValues(trimmedValue, qualifierValues, QUALIFIER_GROUP)
        val certainties = extractValues(qualifiers.reducedInput, certaintyValues, CERTAINTY_GROUP)

        // removing all spaces inside of dates that could throw off the matching. All qualifiers & certainty stem words
        // should already be gone at this stage. There will be no matches if anything is left at this stage.
        val withoutSpaces = certainties.reducedInput.replace(" ", "")

        for (matcher in singleDateMatchers) {
            val normalizedSingleDate = normalizedSingleDate(withoutSpaces, matcher)
            if (normalizedSingleDate.success) {
                createSingleDateResource(item,
                    qualifiers.matchedValues,
                    certainties.matchedValues,
                    normalizedSingleDate.date)
                return emptyList()
            }
        }

        for (matcher in dateRangeMatchers) {
            val match = matcher.matchEntire(withoutSpaces)
            if (match != null) {
                val result = extractDateRange(withoutSpaces, match)
                if (result.success) {
                    createNormalizedDateRangeResource(
                        item,
                        qualifiers.matchedValues,
                        certainties.matchedValues,
                        result.date
                    )
                    return emptyList()
                }
            }

        }
        // even if there is no match, the rdf:type is still replaced with rico:DateRange. See documentation.
        item.replaceRdfType(RICO.DateRange)
        return listOf("The date value '$originalDateValue' could not be normalized.")
    }

    private fun createNormalizedDateRangeResource(
        item: RicoResource,
        qualifiers: List<String>,
        certainties: List<String>,
        normalizedDate: String,
    ) {
        qualifiers.forEach {
            item.addLiteral(RICO.dateQualifier, it)
        }
        certainties.forEach {
            item.addLiteral(RICO.certainty, it)
        }
        item.removeAllProperties(RICO.expressedDate)
        item.addLiteral(RICO.normalizedDateValue, normalizedDate)
        item.replaceRdfType(RICO.DateRange)
    }

    private data class ExtractedValues(
        val reducedInput: String,
        val matchedValues: List<String>,
    )

    private fun extractValues(inputValue: String, expressions: List<Regex>, group: String): ExtractedValues {
        val matches = mutableListOf<MatchResult>()
        for (regex in expressions) {
            matches.addAll(regex.findAll(inputValue))
        }

        var reducedInput = inputValue
        val matchedValues = mutableListOf<String>()
        for (match in matches) {
            val matchedValue = match.groups[group]?.value
            if (matchedValue != null) {
                reducedInput = reducedInput.replace(match.groups[0]?.value!!, "").trim()
                matchedValues.add(matchedValue.trim())
            }
        }
        return ExtractedValues(reducedInput, matchedValues)
    }

    private fun normalizedSingleDate(originalDateValue: String, regex: Regex): Date.Result {
        val match = regex.matchEntire(originalDateValue)
            ?: return Date.Result(
                date = "",
                success = false,
                message = "Could not match single date value '$originalDateValue' with pattern '${regex.pattern}'.",
            )

        val day = match.groups[DAY_GROUP]?.value.let { day ->
            if (day == null)
                return@let null

            val result = Date.normalizeDayValue(day)
            if (result.success) {
                result.date
            } else {
                null
            }
        }
        val month = match.groups[MONTH_GROUP]?.value.let { month ->
            if (month == null) {
                return@let null
            }
            val result = Date.normalizeMonthValue(month)
            if (result.success) {
                result.date
            } else {
                null
            }
        }
        val year = match.groups[YEAR_GROUP]?.value
        return if (day != null && month != null && year != null) {
            Date.Result(
                date = normalizedSingleDate(day, month, year),
                success = true,
                message = "",
            )
        } else {
            Date.Result(
                date = "",
                success = false,
                message = "Could not extract single date value: $originalDateValue."
            )
        }

    }

    private fun normalizedSingleDate(day: String, month: String, year: String): String {
        return "$year-$month-$day"
    }

    private fun extractDateRange(inputValue: String, matchResult: MatchResult): Date.Result {
        if (matchResult.groups.isEmpty())
            return Date.Result(
                date = "",
                success = false,
                message = "No matches found."
            )

        val singleYear = try {
            matchResult.groups[SINGLE_YEAR_GROUP]?.value
        } catch (ex: IllegalArgumentException) {
            null
        }
        val singleMonth = try {
            val month = matchResult.groups[SINGLE_MONTH_GROUP]?.value
            if (month != null) {
                val result = Date.normalizeMonthValue(month)
                if (result.success) {
                    result.date
                } else {
                    null
                }
            } else {
                null
            }
        } catch (ex: IllegalArgumentException) {
            null
        }
        val untilYear = try {
            matchResult.groups[YEAR_UNTIL_GROUP]?.value
        } catch (ex: IllegalArgumentException) {
            null
        }
        val fromYear = try {
            matchResult.groups[YEAR_FROM_GROUP]?.value
        } catch (ex: IllegalArgumentException) {
            null
        }
        val untilMonth = try {
            val month = matchResult.groups[MONTH_UNTIL_GROUP]?.value
            if (month != null) {
                val result = Date.normalizeMonthValue(month)
                if (result.success) {
                    result.date
                } else {
                    null
                }
            } else {
                null
            }
        } catch (ex: IllegalArgumentException) {
            null
        }
        val fromMonth = try {
            val month = matchResult.groups[MONTH_FROM_GROUP]?.value
            if (month != null) {
                val result = Date.normalizeMonthValue(month)
                if (result.success) {
                    result.date
                } else {
                    null
                }
            } else {
                null
            }
        } catch (ex: IllegalArgumentException) {
            null
        }
        val untilDay = try {
            val day = matchResult.groups[DAY_UNTIL_GROUP]?.value
            if (day != null) {
                val result = Date.normalizeDayValue(day)
                if (result.success) {
                    result.date
                } else {
                    null
                }
            } else {
                null
            }
        } catch (ex: IllegalArgumentException) {
            null
        }
        val fromDay = try {
            val day = matchResult.groups[DAY_FROM_GROUP]?.value
            if (day != null) {
                val result = Date.normalizeDayValue(day)
                if (result.success) {
                    result.date
                } else {
                    null
                }
            } else {
                null
            }
        } catch (ex: IllegalArgumentException) {
            null
        }
        return normalizeDateRange(
            originalDateValue = inputValue,
            dayFrom = fromDay,
            dayUntil = untilDay,
            monthFrom = fromMonth,
            monthUntil = untilMonth,
            yearFrom = fromYear,
            yearUntil = untilYear,
            singleMonth = singleMonth,
            singleYear = singleYear
        )
    }

    private fun normalizeDateRange(
        originalDateValue: String,
        dayFrom: String?,
        dayUntil: String?,
        monthFrom: String?,
        monthUntil: String?,
        yearFrom: String?,
        yearUntil: String?,
        singleMonth: String?,
        singleYear: String?,
    ): Date.Result {
        return when {
            singleYear != null ->
                when {
                    singleMonth != null ->
                        when {
                            dayFrom != null && dayUntil != null -> Date.Result(
                                date = "$singleYear-$singleMonth-$dayFrom/$dayUntil",
                                success = true, message = ""
                            )

                            else -> Date.Result(
                                date = "$singleYear-$singleMonth",
                                success = true, message = ""
                            )
                        }

                    monthFrom != null && monthUntil != null ->
                        when {
                            dayFrom != null && dayUntil != null -> Date.Result(
                                date = "$singleYear-$monthFrom-$dayFrom/$monthUntil-$dayUntil",
                                success = true, message = ""
                            )

                            else -> Date.Result(
                                date = "$singleYear-$monthFrom/$monthUntil",
                                success = true, message = ""
                            )
                        }

                    else -> Date.Result(
                        date = singleYear,
                        success = true, message = ""
                    )
                }

            yearFrom != null && yearUntil != null ->
                when {
                    monthFrom != null && monthUntil != null ->
                        when {
                            dayFrom != null && dayUntil != null -> Date.Result(
                                date = "$yearFrom-$monthFrom-$dayFrom/$yearUntil-$monthUntil-$dayUntil",
                                success = true, message = ""
                            )


                            else -> Date.Result(
                                date = "$yearFrom-$monthFrom/$yearUntil-$monthUntil",
                                success = true, message = ""
                            )
                        }

                    else -> Date.Result(
                        date = "$yearFrom/$yearUntil",
                        success = true, message = ""
                    )
                }

            else -> {
                Date.Result(
                    date = "",
                    success = false,
                    message = "Could not extract date range from $originalDateValue.",
                )
            }
        }
    }

    private fun createSingleDateResource(item: RicoResource, qualifiers: List<String>, certainties: List<String>, date: String) {
        item
            .removeAllProperties(RICO.expressedDate)
            .addLiteral(RICO.normalizedDateValue, date)
            .removeAllProperties(RDF.type)
            .replaceRdfType(RICO.SingleDate)

        qualifiers.forEach {
            item.addLiteral(RICO.dateQualifier, it)
        }
        certainties.forEach {
            item.addLiteral(RICO.certainty, it)
        }
    }

    override fun hashCode(): Int {
        var result = singleDateMatchers.hashCode()
        result = 31 * result + dateRangeMatchers.hashCode()
        result = 31 * result + certaintyValues.hashCode()
        result = 31 * result + qualifierValues.hashCode()
        return result
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DateNormalizationTransform

        if (singleDateMatchers != other.singleDateMatchers) return false
        if (dateRangeMatchers != other.dateRangeMatchers) return false
        if (certaintyValues != other.certaintyValues) return false
        if (qualifierValues != other.qualifierValues) return false

        return true
    }
}

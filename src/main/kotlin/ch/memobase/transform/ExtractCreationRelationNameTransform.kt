/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.transform

import ch.memobase.helpers.Values.EXTRACT_RELATION_NAME_GROUP_PATTERN_NAME
import ch.memobase.rdf.MemobaseModel
import ch.memobase.rdf.RICO
import ch.memobase.rdf.RicoResource
import org.apache.logging.log4j.LogManager

class ExtractCreationRelationNameTransform(private val regex: Regex, private val language: String) : ITransformer {
    private val log = LogManager.getLogger(this::class.java)

    override fun transform(item: RicoResource, record: RicoResource, model: MemobaseModel): List<String> {
        if (item.hasType(RICO.CreationRelation)) {
            val target = item.getResource(RICO.creationRelationHasTarget)
            if (target.hasType(RICO.Person) && target.hasProperty(RICO.name)) {
                val updatedNames = target.listProperties(RICO.name).mapNotNull { statement ->
                    if (statement.`object`.isLiteral) {
                        val value = statement.`object`.asLiteral()
                        val fullValue = value.string
                        val match = regex.find(fullValue)
                        val targetValue = if (match != null) {
                            match.groups[EXTRACT_RELATION_NAME_GROUP_PATTERN_NAME]?.value.let { relation ->
                                if (relation != null) {
                                    // add names to creation relation!
                                    if (value.language.isNullOrEmpty()) {
                                        item.addLiteral(RICO.name, relation.trim(), language)
                                    } else {
                                        item.addLiteral(RICO.name, relation.trim(), value.language)
                                    }
                                    fullValue.replace(match.value, "")
                                } else {
                                    fullValue
                                }
                            }
                        } else {
                            log.debug("Pattern '${regex.pattern}' did not match on value $fullValue of Person.")
                            fullValue
                        }
                        Pair(targetValue, value.language)
                    } else {
                        null
                    }
                }
                target.removeAllProperties(RICO.name)
                updatedNames.forEach { pair ->
                    target.addLiteral(RICO.name, pair.first.trim(), pair.second.trim())
                }
            }
        }
        return emptyList()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ExtractCreationRelationNameTransform

        if (regex.pattern != other.regex.pattern) return false
        if (language != other.language) return false

        return true
    }

    override fun hashCode(): Int {
        var result = regex.hashCode()
        result = 31 * result + language.hashCode()
        return result
    }
}

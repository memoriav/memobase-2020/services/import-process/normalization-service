/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.transform

import ch.memobase.helpers.Activity
import ch.memobase.helpers.Values
import ch.memobase.schema.Facets
import ch.memobase.schema.Labels
import ch.memobase.rdf.EBUCORE
import ch.memobase.rdf.EnrichedConceptBuilder
import ch.memobase.rdf.MemobaseModel
import ch.memobase.rdf.RDF
import ch.memobase.rdf.RICO
import ch.memobase.rdf.RicoResource
import ch.memobase.rdf.SKOS
import org.apache.logging.log4j.LogManager
import java.util.*

class GenreNormalizer(
    private val facetsMap: Map<String, Facets>,
    private val labelsMap: Map<String, Labels>
) : ITransformer {
    private val log = LogManager.getLogger(this::class.java)

    override fun transform(item: RicoResource, record: RicoResource, model: MemobaseModel): List<String> {
        return if (item.hasType(SKOS.Concept)) {
            if (record.hasProperty(EBUCORE.hasGenre, item)) {
                if (item.hasProperty(SKOS.prefLabel)) {
                    val allStatements = item.listProperties()
                    val copyStatements =
                        allStatements.filter { it.predicate != SKOS.prefLabel }.filter { it.predicate != RDF.type }
                    item.listProperties(SKOS.prefLabel).mapNotNull { statement ->
                        val nameValue = statement.string.trim()
                        val lowerCaseValue = nameValue.lowercase(Locale.getDefault())
                        val builder = EnrichedConceptBuilder(SKOS.Concept, model, record, EBUCORE.hasGenre)
                        facetsMap[lowerCaseValue].let { facetValues ->
                            if (facetValues != null) {
                                val activity = Activity.createActivity(Values.GENRE_NORMALIZER_MECHANISM_NAME, model)
                                activity.addProperty(RICO.affectsOrAffected, item.resource)
                                item.resource.addProperty(RICO.isOrWasAffectedBy, activity)

                                facetValues.wikidata.forEach { facetValue ->
                                    builder
                                        .init()
                                        .addLabels(SKOS.prefLabel, labelsMap.getOrDefault(facetValue, Labels.default))
                                        .addSameAsLink(facetValue)
                                        .addOtherStatements(copyStatements)
                                        .appendExistingActivity(activity)
                                        .build()
                                }
                                facetValues.freeFacetValue.forEach { facetValue ->
                                    builder
                                        .init()
                                        .addLabels(SKOS.prefLabel, labelsMap.getOrDefault(facetValue, Labels.default))
                                        .addOtherStatements(copyStatements)
                                        .appendExistingActivity(activity)
                                        .build()
                                }
                                null
                            } else {
                                log.error("Could not map genre with value: $nameValue.")
                                "Could not map the genre with value: $nameValue."
                            }
                        }
                    }
                } else {
                    log.error("Genre without a name found: $item}.")
                    listOf("Genre without a name found: $item.")
                }
            } else {
                emptyList()
            }
        } else {
            emptyList()
        }
    }
}
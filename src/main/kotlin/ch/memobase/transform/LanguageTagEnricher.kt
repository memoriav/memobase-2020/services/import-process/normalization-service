/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.transform

import ch.memobase.rdf.MemobaseModel
import ch.memobase.rdf.RicoResource
import org.apache.jena.rdf.model.Literal
import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource
import org.apache.jena.rdf.model.impl.LiteralImpl

class LanguageTagEnricher(
    val language: String,
    val properties: Map<Resource, List<Property>>
): ITransformer {

    override fun transform(item: RicoResource, record: RicoResource, model: MemobaseModel): List<String> {
        val messages = mutableListOf<String>()
        properties[item.getType()].let { propertyList ->
            propertyList?.forEach { property ->
                val items = item.listProperties(property).toList()
                if (items.isNotEmpty()) {
                    val hasLiteralWithLanguageTag = items.any { statement ->
                        val obj = statement.`object`
                        obj is Literal && (obj as LiteralImpl).language != null && obj.language != ""
                    }
                    if (!hasLiteralWithLanguageTag) {
                        items.forEach {statement ->
                            val obj = statement.`object`
                            if (obj is Literal) {
                                val oldProp = statement.predicate
                                val literal = obj as LiteralImpl
                                item.removeStatement(statement)
                                item.addLiteral(oldProp, literal.lexicalForm, language)
                            }
                        }
                    }
                }
            }
        }
        return messages
    }
}
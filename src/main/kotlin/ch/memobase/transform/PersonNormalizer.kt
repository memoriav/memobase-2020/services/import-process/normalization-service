/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.transform

import ch.memobase.helpers.Values
import ch.memobase.rdf.MemobaseModel
import ch.memobase.rdf.RICO
import ch.memobase.rdf.RicoResource
import org.apache.jena.sparql.vocabulary.FOAF
import org.apache.logging.log4j.LogManager

class PersonNormalizer(
    private val nameOrder: String,
    private val singleNameIsLastName: Boolean,
    private val nameDelimiter: String
) : ITransformer {
    private val log = LogManager.getLogger(this::class.java)

    override fun transform(item: RicoResource, record: RicoResource, model: MemobaseModel): List<String> {
        return if (item.hasType(RICO.Person) && !item.hasProperty(RICO.type)) {
            val names = item.listProperties(RICO.name).toList()
            names.forEach { statement ->
                val value = statement.`object`.asLiteral()
                val text = value.string.trim()
                val splitNames = if (nameOrder == Values.FIRST_TO_LAST_NAME && nameDelimiter == " ") {
                    listOf(
                        text.substringBeforeLast(
                            nameDelimiter,
                            missingDelimiterValue = if (singleNameIsLastName) "" else text
                        ).trim(),
                        text.substringAfterLast(
                            nameDelimiter,
                            missingDelimiterValue = if (singleNameIsLastName) text else ""
                        ).trim()
                    ).filter { it != "" }
                } else {
                    text.split(nameDelimiter).map { it.trim() }
                }
                if (splitNames.isEmpty()) {
                    log.error("The split names list is empty: $text.")
                } else if (splitNames.size == 1) {
                    if (singleNameIsLastName) {
                        item.addLiteral(FOAF.lastName, splitNames[0], value.language)
                    } else {
                        item.addLiteral(FOAF.firstName, splitNames[0], value.language)
                    }
                } else {
                    if (nameOrder == Values.FIRST_TO_LAST_NAME) {
                        item.addLiteral(FOAF.firstName, splitNames[0], value.language)
                        item.addLiteral(FOAF.lastName, splitNames[1], value.language)
                    } else {
                        item.addLiteral(FOAF.lastName, splitNames[0], value.language)
                        item.addLiteral(FOAF.firstName, splitNames[1], value.language)
                    }
                }
            }
            emptyList()
        } else {
            emptyList()
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PersonNormalizer

        if (nameOrder != other.nameOrder) return false
        if (singleNameIsLastName != other.singleNameIsLastName) return false
        if (nameDelimiter != other.nameDelimiter) return false

        return true
    }

    override fun hashCode(): Int {
        var result = nameOrder.hashCode()
        result = 31 * result + singleNameIsLastName.hashCode()
        result = 31 * result + nameDelimiter.hashCode()
        return result
    }
}

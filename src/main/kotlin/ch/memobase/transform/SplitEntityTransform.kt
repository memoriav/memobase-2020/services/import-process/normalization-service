/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.transform

import ch.memobase.rdf.MemobaseModel
import ch.memobase.rdf.RICO
import ch.memobase.rdf.RicoResource
import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource
import org.apache.jena.rdf.model.impl.StatementImpl

class SplitEntityTransform(private val type: Resource, private val literal: Property, private val delimiter: String) :
    ITransformer {
    private var oldCreationRelation: Resource? = null

    override fun transform(item: RicoResource, record: RicoResource, model: MemobaseModel): List<String> {
        if (!item.hasType(type))
            return emptyList()

        if (!item.hasProperty(literal))
            return emptyList()

        val originalValue = item.getStringLiteral(literal).orEmpty()
        val language = item.getLiteralLanguage(literal)
        val newValues = originalValue.split(delimiter)
            .map { value -> value.trim() }
            .filter { value -> value.isNotEmpty() }
        val hasCreationRelation = item.hasProperty(RICO.agentIsTargetOfCreationRelation)
        if (hasCreationRelation) {
            for (value in newValues) {
                val newEntity = model.createResource()
                for (statement in item.listProperties()) {
                    when (statement.predicate) {
                        literal -> {
                            newEntity.addProperty(statement.predicate, model.createLiteral(value, language))
                        }

                        RICO.agentIsTargetOfCreationRelation -> {
                            val newCreationRelation = model.createResource()
                            oldCreationRelation = statement.`object`.asResource()
                            for (oldCreationRelationStatements in oldCreationRelation!!.listProperties()) {
                                when (oldCreationRelationStatements.predicate) {
                                    RICO.creationRelationHasTarget ->
                                        newCreationRelation.addProperty(
                                            RICO.creationRelationHasTarget,
                                            newEntity
                                        )

                                    else ->
                                        newCreationRelation.addProperty(
                                            oldCreationRelationStatements.predicate,
                                            oldCreationRelationStatements.`object`
                                        )
                                }
                            }
                            newEntity.addProperty(RICO.agentIsTargetOfCreationRelation, newCreationRelation)
                            record.addProperty(
                                RICO.recordResourceOrInstantiationIsSourceOfCreationRelation,
                                newCreationRelation
                            )
                        }

                        else -> {
                            newEntity.addProperty(statement.predicate, statement.`object`)
                        }
                    }
                }
            }
            model.remove(
                StatementImpl(
                    record.resource,
                    RICO.recordResourceOrInstantiationIsSourceOfCreationRelation,
                    oldCreationRelation
                )
            )
            model.remove(oldCreationRelation!!.listProperties())
            oldCreationRelation = null
        } else {
            // this is done this way around because not all resources have an inverse property that connect them to
            // the record, but the record is always connected to the item.
            val recordToItemStatements = record.resource.listProperties().filterKeep {
                val objectResource = it.`object`
                if (objectResource.isResource) {
                    objectResource.asResource() == item.resource
                } else {
                    false
                }
            }.toList()
            val recordToItemStatement = recordToItemStatements[0]
            for (value in newValues) {
                val blank = model.createResource()
                for (statement in item.listProperties()) {
                    when (statement.predicate) {
                        literal -> {
                            blank.addProperty(statement.predicate, model.createLiteral(value, language))
                        }

                        else -> {
                            blank.addProperty(statement.predicate, statement.`object`)
                        }
                    }
                }
                record.addProperty(recordToItemStatement.predicate, blank)
            }
            model.remove(recordToItemStatement)
        }
        model.remove(item.listProperties().toList())
        return emptyList()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SplitEntityTransform

        if (type != other.type) return false
        if (literal != other.literal) return false
        if (delimiter != other.delimiter) return false

        return true
    }

    override fun hashCode(): Int {
        var result = type.hashCode()
        result = 31 * result + literal.hashCode()
        result = 31 * result + delimiter.hashCode()
        return result
    }
}

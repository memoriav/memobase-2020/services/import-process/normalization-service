/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.transform

import ch.memobase.helpers.Activity
import ch.memobase.helpers.RequestClient
import ch.memobase.helpers.Values
import ch.memobase.helpers.WikidataToMemobase
import ch.memobase.rdf.*
import ch.memobase.schema.external.WikidataLinkerValueType
import org.apache.logging.log4j.LogManager

class WikidataLinksEnricher(
    private val recordSetId: String,
    private val client: RequestClient,
) : ITransformer {
    private val log = LogManager.getLogger(this::class.java)

    private val placeProperties =
        listOf(
            WD.INSTANCE_OF_P31,
            WD.GND_ID_P227,
            WD.COORDINATE_LOCATION_P625,
            WD.HDS_ID_P902,
            WD.COORDINATES_OF_GEOGRAPHIC_CENTER_P5140,
        )

    private val personProperties = listOf(
        WD.INSTANCE_OF_P31,
        WD.GND_ID_P227,
        WD.HDS_ID_P902,
    )

    companion object {
        const val WIKIDATA_BASE_URL = "https://www.wikidata.org/entity"
        const val GND_BASE_URL = "https://d-nb.info/gnd"
        const val HLS_BASE_URL = "https://hls-dhs-dss.ch/articles"
    }

    override fun transform(item: RicoResource, record: RicoResource, model: MemobaseModel): List<String> {
        val issues = mutableListOf<String>()
        if (item.hasType(RICO.Place)) {
            issues.addAll(linkPlaces(item, record, model))
        }
        if (item.hasType(RICO.Person)) {
            issues.addAll(linkAgents(item, record, model))
        }
        return issues
    }


    fun enrichFreetextEntities(record: RicoResource, model: MemobaseModel): List<String> {
        val issues = mutableListOf<String>()
        val recordId = model.getRecordId()
        val response = client.requestConnectomData(recordId)
        if (response.success) {
            val activity = Activity.createActivity(Values.MECHANISM_NAME_ENRICHED_BY_CONNECTOM, model)

            for (entity in response.spatials) {
                val wikidataUri = entity.sameAs.first()
                val id = wikidataUri.split("Q")[1].toInt()
                val metadata = client.requestWikidataMetadata(id, placeProperties)

                if (metadata.error != null) {
                    log.warn(metadata.error)
                    continue
                }

                WikidataToMemobase.transformResponse(
                    model,
                    RICO.Place,
                    Values.ENTITY_RICO_TYPE_ENRICHED_BY_CONNECTOM,
                    metadata,
                    issues,
                    id,
                )?.let { newResource ->
                    record.addProperty(DC.spatial, newResource)
                    activity.addProperty(RICO.affectsOrAffected, record.resource)
                    record.addProperty(RICO.isOrWasAffectedBy, activity)
                    activity.addProperty(RICO.resultsOrResultedIn, newResource.resource)
                    newResource.addProperty(RICO.resultsOrResultedFrom, activity)
                }
            }

            for (entity in response.agents) {
                val wikidataUri = entity.sameAs.first()
                val id = wikidataUri.split("Q")[1].toInt()
                val metadata = client.requestWikidataMetadata(id, personProperties)

                if (metadata.error != null) {
                    log.warn(metadata.error)
                    continue
                }

                WikidataToMemobase.transformResponse(
                    model,
                    RICO.Person,
                    Values.ENTITY_RICO_TYPE_ENRICHED_BY_CONNECTOM,
                    metadata,
                    issues,
                    id,
                )?.let { newResource ->
                    record.addProperty(RICO.hasOrHadSubject, newResource)
                    newResource.addProperty(RICO.isOrWasSubjectOf, record)
                    activity.addProperty(RICO.affectsOrAffected, record.resource)
                    record.addProperty(RICO.isOrWasAffectedBy, activity)
                    activity.addProperty(RICO.resultsOrResultedIn, newResource.resource)
                    newResource.addProperty(RICO.resultsOrResultedFrom, activity)
                }
            }
        }
        issues.add(response.id)
        return issues
    }


    private fun linkPlaces(item: RicoResource, record: RicoResource, model: MemobaseModel): List<String> {
        val issues = mutableListOf<String>()
        val allStatements = item.listProperties(RICO.name).toList()
        if (allStatements.isNotEmpty()) {
            val values = allStatements.map { it.string.trim() }
            for (value in values) {
                val response = client.requestWikidataLink(value, recordSetId, WikidataLinkerValueType.LOCATION)
                if (response.extractor == "Error") {
                    issues.add(response.value)
                    continue
                }
                if (response.extractor == "No matches found") {
                    issues.add("No matches found for value $value.")
                    continue
                }

                if (response.matches.isNotEmpty()) {

                    val activity = Activity.createActivity(Values.MECHANISM_NAME_WIKIDATA_LINKER_PLACE, model)

                    for (match in response.matches) {
                        val metadata = client.requestWikidataMetadata(match.qid, placeProperties)

                        if (metadata.error != null) {
                            log.warn(metadata.error)
                            continue
                        }

                        WikidataToMemobase.transformResponse(
                            model,
                            RICO.Place,
                            Values.ENTITY_RICO_TYPE_ENRICHED_BY_MEMOBASE,
                            metadata,
                            issues,
                            match.qid,
                        )?.let { newResource ->

                            for (statement in record.listProperties(DC.spatial).toList()) {
                                if (statement.resource.uri == newResource.resource.uri) {
                                    record.addProperty(DC.spatial, newResource)
                                }
                            }

                            for (statement in record.listProperties(RDA.hasPlaceOfCapture).toList()) {
                                if (statement.resource.uri == newResource.resource.uri) {
                                    record.addProperty(RDA.hasPlaceOfCapture, newResource)
                                    newResource.addProperty(RDA.isPlaceOfCaptureOf, record)
                                }
                            }

                            activity.addProperty(RICO.affectsOrAffected, item.resource)
                            item.addProperty(RICO.isOrWasAffectedBy, activity)
                            activity.addProperty(RICO.resultsOrResultedIn, newResource.resource)
                            newResource.addProperty(RICO.resultsOrResultedFrom, activity)
                        }
                    }
                }
            }
            return issues
        } else {
            return listOf("Place without a name found: $item.")
        }
    }

    private fun linkAgents(item: RicoResource, record: RicoResource, memobaseModel: MemobaseModel): List<String> {
        val issues = mutableListOf<String>()
        val allStatements = item.listProperties(RICO.name).toList()
        if (allStatements.isNotEmpty()) {
            val values = allStatements.map { it.string.trim() }
            for (value in values) {
                val response = client.requestWikidataLink(value, recordSetId, WikidataLinkerValueType.AGENT)
                if (response.extractor == "Error") {
                    issues.add(response.value)
                    continue
                }
                if (response.extractor == "No matches found") {
                    issues.add("No matches found for value $value.")
                    continue
                }

                if (response.matches.isNotEmpty()) {
                    val activity = Activity.createActivity(Values.MECHANISM_NAME_WIKIDATA_LINKER_AGENT, memobaseModel)
                    for (match in response.matches) {
                        val metadata = client.requestWikidataMetadata(match.qid, personProperties)

                        if (metadata.error != null) {
                            log.warn(metadata.error)
                            continue
                        }

                        WikidataToMemobase.transformResponse(
                            memobaseModel,
                            RICO.Person,
                            Values.ENTITY_RICO_TYPE_ENRICHED_BY_MEMOBASE,
                            metadata,
                            issues,
                            match.qid,
                        )?.let { newResource ->
                            record.addProperty(RICO.hasOrHadSubject, newResource)
                            newResource.addProperty(RICO.isOrWasSubjectOf, record)

                            activity.addProperty(RICO.affectsOrAffected, item.resource)
                            item.addProperty(RICO.isOrWasAffectedBy, activity)
                            activity.addProperty(RICO.resultsOrResultedIn, newResource.resource)
                            newResource.addProperty(RICO.resultsOrResultedFrom, activity)
                        }
                    }
                } else {
                    issues.add("No matches found for value $value.")
                }
            }
        }
        return issues
    }
}

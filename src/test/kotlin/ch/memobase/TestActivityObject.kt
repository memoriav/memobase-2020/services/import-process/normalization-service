/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.helpers.Activity
import ch.memobase.rdf.MemobaseModel
import ch.memobase.rdf.RICO
import org.apache.jena.rdf.model.Statement
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestActivityObject {

    @Test
    fun `test activity object creation`() {
        val memobaseModel = MemobaseModel()

        val sourceCarrierType = memobaseModel.createRicoResource(RICO.CarrierType)
            .addLiteral(RICO.name, "source")
        val carrierType = memobaseModel.createRicoResource(RICO.CarrierType)
            .addLiteral(RICO.name, "normalized")
        Activity.appendActivity(carrierType, "testMechanism", sourceCarrierType, memobaseModel)

        println(memobaseModel)
    }
}

/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.configs.GlobalTransformsLoader
import ch.memobase.helpers.Date
import ch.memobase.rdf.MemobaseModel
import ch.memobase.rdf.RICO
import ch.memobase.rdf.RicoResource
import ch.memobase.transform.DateNormalizationTransform
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Condition
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestDates {

    @Test
    fun `test month symbol map`() {
        val dez = Date.normalizeMonthValue("Dezember")
        val feb = Date.normalizeMonthValue("févr.")
        val march = Date.normalizeMonthValue("5")

        assertAll("",
            {
                assertThat(dez.date)
                    .isEqualTo("12")
            },
            {
                assertThat(feb.date)
                    .isEqualTo("02")
            },
            {
                assertThat(march.date)
                    .isEqualTo("05")
            }
        )

    }

    @ParameterizedTest
    @MethodSource("testDates")
    fun `test year range normalization`(dateParams: DateParams) {
        val model = MemobaseModel()
        val resource = RicoResource(
            model.createResource(),
            RICO.DateSet
        )
        resource.addLiteral(RICO.expressedDate, dateParams.input)
        val transforms = GlobalTransformsLoader.parse(TestUtils.GLOBAL_TRANSFORM_FILE)
        val dateNormalizer = transforms.first { it is DateNormalizationTransform }
        dateNormalizer as DateNormalizationTransform

        val issues = dateNormalizer.transform(resource, RicoResource(model.createResource(), RICO.Record), model)

        assertAll(
            "",
            {
                assertThat(resource)
                    .satisfies(
                        Condition(
                            {
                                it.resource.hasProperty(RICO.normalizedDateValue) && it.resource.getProperty(
                                    RICO.normalizedDateValue
                                ).string == dateParams.expectedOutput
                            }, "The normalized date value is the same as the expected output."
                        )
                    )
            },
            {
                assertThat(issues).isEqualTo(dateParams.issueMessages)
            }
        )
    }

    private fun testDates() = Stream.of(
        DateParams(
            "1939",
            "1939",
            emptyList()
        ),
        DateParams(
            "April1939",
            "1939-04",
            emptyList()
        ),
        DateParams(
            "April-September1939",
            "1939-04/09",
            emptyList()
        ),
        DateParams(
            "1.-8.10.1939",
            "1939-10-01/08",
            emptyList()
        ),
        DateParams(
            "01.02.-08.10.1939",
            "1939-02-01/10-08",
            emptyList()
        ),
        DateParams(
            "1987-1994",
            "1987/1994",
            emptyList()
        ),
        DateParams(
            "April1987-Oktober2001",
            "1987-04/2001-10",
            emptyList()
        ),
        DateParams(
            "01.April1987-05.Oktober2001",
            "1987-04-01/2001-10-05",
            emptyList()
        ),
        DateParams(
            "12.1948",
            "1948-12",
            emptyList()
        ),
        DateParams(
            "Août-décembre 1996",
            "1996-08/12",
            emptyList()
        ),
        DateParams(
            "ca. 01.10.1996",
            "1996-10-01",
            emptyList()
        )
    )

}
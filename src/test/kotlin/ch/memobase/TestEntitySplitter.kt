/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.rdf.MemobaseModel
import ch.memobase.rdf.RDF
import ch.memobase.rdf.RICO
import ch.memobase.rdf.RicoResource
import ch.memobase.rdf.SKOS
import ch.memobase.transform.SplitEntityTransform
import java.io.File
import java.nio.charset.Charset
import java.util.stream.Stream
import org.apache.logging.log4j.LogManager
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import ch.memobase.params.EntitySplitterParams


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestEntitySplitter {
    private val log = LogManager.getLogger("TransformerTests")

    private val resourcePath = "src/test/resources/data"
    private fun readFile(fileName: String): String {
        return File("$resourcePath/$fileName").readText(Charset.defaultCharset())
    }

    @ParameterizedTest
    @MethodSource("entitySplitterParams")
    fun `test entity splitter simple`(params: EntitySplitterParams) {
        val entitySplitter = SplitEntityTransform(params.type, params.splitterLiteral, params.delimiter)
        val model = MemobaseModel()
        val record = model.createRicoResource(RICO.Record)
        val splitResource =
                model.createRicoResource(params.type)
                        .addLiteral(params.splitterLiteral, params.value)

        record.addProperty(params.recordToEntity, splitResource.resource)

        val result = entitySplitter.transform(splitResource, record, model)

        val results = model.listSubjectsWithProperty(RDF.type, params.type).mapWith { RicoResource(it) }.toList()
        results.sortBy { value -> value.getStringLiteral(params.splitterLiteral) }

        assertAll("entity splitter tests",
                { assertThat(results.size).isEqualTo(2) },
                { assertThat(results[0].hasProperty(params.splitterLiteral, params.splitValues[0])).isTrue() },
                { assertThat(results[1].hasProperty(params.splitterLiteral, params.splitValues[1])).isTrue() },
                { assertThat(record.listProperties(params.recordToEntity).toList().size).isEqualTo(2) },
                { assertThat(result).isEmpty() }
        )
    }

    fun entitySplitterParams(): Stream<EntitySplitterParams> = Stream.of(
            EntitySplitterParams(
                    RICO.Language,
                    RICO.name,
                    ",",
                    RICO.hasOrHadLanguage,
                    "de,fr",
                    listOf("de", "fr")
            ),
            EntitySplitterParams(
                    SKOS.Concept,
                    SKOS.prefLabel,
                    ";",
                    RICO.hasOrHadSubject,
                    "POLITIK, INTERNATIONALE ORGANISATION; GESUNDHEIT, KRANKHEIT, EPIDEMIE",
                    listOf("GESUNDHEIT, KRANKHEIT, EPIDEMIE", "POLITIK, INTERNATIONALE ORGANISATION")
            )
    )

    @Test
    fun `test entity splitter with creation relation`() {
        val entitySplitter = SplitEntityTransform(RICO.Person, RICO.name, ";")

        val model = MemobaseModel()
        val record = model.createRicoResource(RICO.Record)

        val splitResource =
                model.createRicoResource(RICO.Person)
                        .addLiteral(RICO.name, "Test Person 1; Test Person 2")

        val creationRelation =
                model.createRicoResource(RICO.CreationRelation, "creator")
                        .addProperty(RICO.creationRelationHasTarget, splitResource.resource)
                        .addProperty(RICO.creationRelationHasSource, record.resource)
                        .addLiteral(RICO.name, "Fotograf")

        splitResource.addProperty(RICO.agentIsTargetOfCreationRelation, creationRelation.resource)
        record.addProperty(RICO.recordResourceOrInstantiationIsSourceOfCreationRelation, creationRelation.resource)

        val result = entitySplitter.transform(splitResource, record, model)

        val results = model.listSubjectsWithProperty(RDF.type, RICO.Person).toList()
        results.sortBy { value -> value.getProperty(RICO.name).string }

        assertAll("entity splitter tests",
                { assertThat(results.size).isEqualTo(2) },
                { assertThat(results[0].hasProperty(RICO.name, "Test Person 1")).isTrue() },
                { assertThat(results[1].hasProperty(RICO.name, "Test Person 2")).isTrue() },
                {
                    assertThat(
                            record.listProperties(RICO.recordResourceOrInstantiationIsSourceOfCreationRelation).toList().size
                    ).isEqualTo(2)
                },
                { assertThat(result).isEmpty() }
        )
    }

    @Test
    fun `test no entity split`() {
        val entitySplitter = SplitEntityTransform(SKOS.Concept, SKOS.prefLabel, ";")
        val model = MemobaseModel()
        val record = model.createRicoResource(RICO.Record)
        val splitResource =
                model.createRicoResource(SKOS.Concept)
                        .addLiteral(SKOS.prefLabel, "subject1, subject2")

        record.addProperty(RICO.hasOrHadSubject, splitResource.resource)


        val result = entitySplitter.transform(splitResource, record, model)

        val results = model.listSubjectsWithProperty(RDF.type, SKOS.Concept).mapWith { RicoResource(it) }.toList()
        results.sortBy { value -> value.getStringLiteral(SKOS.prefLabel) }

        assertAll("entity splitter tests",
                { assertThat(results.size).isEqualTo(1) },
                { assertThat(results[0].hasProperty(SKOS.prefLabel, "subject1, subject2")).isTrue() },
                { assertThat(record.listProperties(RICO.hasOrHadSubject).toList().size).isEqualTo(1) },
                { assertThat(result).isEmpty() }
        )

    }

    @Test
    fun `test double space entity split`() {
        val entitySplitter = SplitEntityTransform(SKOS.Concept, SKOS.prefLabel, " ")
        val model = MemobaseModel()
        val record = model.createRicoResource(RICO.Record)
        val splitResource =
                model.createRicoResource(SKOS.Concept)
                        .addLiteral(SKOS.prefLabel, "subject1  subject2")

        record.addProperty(RICO.hasOrHadSubject, splitResource.resource)

        val result = entitySplitter.transform(splitResource, record, model)

        val results = model.listSubjectsWithProperty(RDF.type, SKOS.Concept).mapWith { RicoResource(it) }.toList()
        results.sortBy { value -> value.getStringLiteral(SKOS.prefLabel) }

        assertAll("entity splitter tests",
                { assertThat(results.size).isEqualTo(2) },
                { assertThat(results[0].hasProperty(SKOS.prefLabel, "subject1")).isTrue() },
                { assertThat(results[1].hasProperty(SKOS.prefLabel, "subject2")).isTrue() },
                { assertThat(record.listProperties(RICO.hasOrHadSubject).toList().size).isEqualTo(2) },
                { assertThat(result).isEmpty() }
        )

    }
}

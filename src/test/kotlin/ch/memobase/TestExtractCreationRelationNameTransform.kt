/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.helpers.Values.EXTRACT_RELATION_NAME_GROUP_PATTERN_NAME
import ch.memobase.rdf.MemobaseModel
import ch.memobase.rdf.NS
import ch.memobase.rdf.RICO
import ch.memobase.rdf.RicoResource
import ch.memobase.transform.ExtractCreationRelationNameTransform
import java.io.FileOutputStream
import java.util.function.Consumer
import org.apache.jena.riot.RDFDataMgr
import org.apache.jena.riot.RDFFormat
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import java.io.File

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class TestExtractCreationRelationNameTransform {

    @BeforeAll
    fun setupTest() {
        val tmpFiles = File("src/test/resources/tmp")
        if (!tmpFiles.exists()) {
            tmpFiles.mkdir()
        }
    }

    @Test
    fun `test extract creation relation name normalizer`() {
        val memobaseModel = MemobaseModel()
        NS.prefixMapping.map {
            memobaseModel.setNsPrefix(it.key, it.value)
        }

        val person = memobaseModel.createRicoResource(RICO.Person)
                .addLiteral(RICO.name, "Markus Mäder (Autor)")

        val resource =
                memobaseModel.createRicoResource(RICO.CreationRelation)
                        .addProperty(RICO.creationRelationHasTarget, person)

        person.addProperty(RICO.agentIsTargetOfCreationRelation, resource)

        val record = memobaseModel.createRicoResource(RICO.Record)
                .addProperty(RICO.recordResourceOrInstantiationIsSourceOfCreationRelation, resource)

        resource
                .addProperty(RICO.creationRelationHasSource, record)

        val n = ExtractCreationRelationNameTransform(
                Regex("\\((?<$EXTRACT_RELATION_NAME_GROUP_PATTERN_NAME>.+)\\)"),
                ""
        )
        val output = n.transform(resource, record, memobaseModel)
        RDFDataMgr.write(
                FileOutputStream("src/test/resources/tmp/turtle-output-extract-creation-relation-name.ttl"),
                memobaseModel,
                RDFFormat.TURTLE_PRETTY
        )

        val resourceReqs = Consumer<RicoResource> { r ->
            val value = r.getStringLiteral(RICO.name)
            assertThat(value)
                    .isEqualTo("Autor")

            val type = r.hasType(RICO.CreationRelation)
            assertThat(type).isTrue()
        }

        assertAll("",
                {
                    assertThat(output).isEmpty()
                },
                {
                    assertThat(resource).satisfies(resourceReqs)
                }
        )
    }

    @Test
    fun `test extract creation relation name normalizer without a match`() {
        val memobaseModel = MemobaseModel()
        NS.prefixMapping.map {
            memobaseModel.setNsPrefix(it.key, it.value)
        }

        val person = memobaseModel.createRicoResource(RICO.Person)
                .addLiteral(RICO.name, "Markus Mäder")

        val resource =
                memobaseModel.createRicoResource(RICO.CreationRelation)
                        .addProperty(RICO.creationRelationHasTarget, person)

        person.addProperty(RICO.agentIsTargetOfCreationRelation, resource)

        val record = memobaseModel.createRicoResource(RICO.Record)
                .addProperty(RICO.recordResourceOrInstantiationIsSourceOfCreationRelation, resource)

        resource
                .addProperty(RICO.creationRelationHasSource, record)

        val n = ExtractCreationRelationNameTransform(
                Regex("\\((?<$EXTRACT_RELATION_NAME_GROUP_PATTERN_NAME>.+)\\)"),
                ""
        )
        val output = n.transform(resource, record, memobaseModel)
        RDFDataMgr.write(
                FileOutputStream("src/test/resources/tmp/turtle-output-extract-creation-relation-name-without-a-match.ttl"),
                memobaseModel,
                RDFFormat.TURTLE_PRETTY
        )

        val resourceReqs = Consumer<RicoResource> { r ->
            val value = r.getStringLiteral(RICO.name)
            assertThat(value)
                    .isEqualTo(null)

            val type = r.hasType(RICO.CreationRelation)
            assertThat(type).isTrue()
        }

        assertAll("",
                {
                    assertThat(output).isEmpty()
                },
                {
                    assertThat(resource).satisfies(resourceReqs)
                }
        )
    }
}
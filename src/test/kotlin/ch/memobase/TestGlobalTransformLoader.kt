/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.configs.GlobalTransformsLoader
import ch.memobase.helpers.GlobalTransformException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS
import org.junit.jupiter.api.assertThrows

@TestInstance(PER_CLASS)
class TestGlobalTransformLoader {

    private val basePath = "src/test/resources/global/tests"

    @Test
    fun `test full valid global loader`() {
        val file = "src/test/resources/global/transforms.yml"
        val transforms = GlobalTransformsLoader.parse(file)
        assertThat(transforms)
            .isNotEmpty
    }

    @Test
    fun `test 1 - invalid transform path 1`() {
        val file = "/test-certs/invalid/path/transforms.yml"
        assertThrows<GlobalTransformException> {
            GlobalTransformsLoader.parse(file)
        }
    }

    @Test
    fun `test 2 - missing date normalization file`() {
        val file = "$basePath/2/transforms.yml"
        assertThrows<GlobalTransformException> {
            GlobalTransformsLoader.parse(file)
        }
    }

    @Test
    fun `test 3 - invalid regex pattern in date`() {
        val file = "$basePath/3/transforms.yml"
        assertThrows<GlobalTransformException> {
            GlobalTransformsLoader.parse(file)
        }
    }

    @Test
    fun `test 4 - missing qualifier group in qualifier regex`() {
        val file = "$basePath/4/transforms.yml"
        assertThrows<GlobalTransformException> {
            GlobalTransformsLoader.parse(file)
        }
    }

    @Test
    fun `test 5 - valid genre transform`() {
        val file = "$basePath/5/transforms.yml"
        assertThrows<GlobalTransformException> {
            GlobalTransformsLoader.parse(file)
        }
    }
}
/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.params.KafkaTestParams
import ch.memobase.reporting.Report
import org.apache.kafka.common.header.internals.RecordHeader
import org.apache.kafka.common.header.internals.RecordHeaders
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.kafka.streams.TopologyTestDriver
import org.apache.kafka.streams.test.TestRecord
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.io.File
import java.nio.charset.Charset
import java.util.stream.Stream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestIntegration {

    private val resourcePath = "src/test/resources/data"
    private fun readFile(fileName: String): String {
        return File("$resourcePath/$fileName").readText(Charset.defaultCharset())
    }

    private fun createHeaders(): RecordHeaders {
        val headers = RecordHeaders()
        headers.add(RecordHeader("sessionId", "test-session-id".toByteArray()))
        headers.add(RecordHeader("recordSetId", "test-record-set-id".toByteArray()))
        headers.add(RecordHeader("institutionId", "test-institution-id".toByteArray()))
        headers.add(RecordHeader("isPublished", "false".toByteArray()))
        headers.add(RecordHeader("xmlRecordTag", "record".toByteArray()))
        headers.add(RecordHeader("xmlIdentifierFieldName", "identifierMain".toByteArray()))
        headers.add(RecordHeader("tableSheetIndex", "1".toByteArray()))
        headers.add(RecordHeader("tableHeaderCount", "1".toByteArray()))
        headers.add(RecordHeader("tableHeaderIndex", "1".toByteArray()))
        headers.add(RecordHeader("tableIdentifierIndex", "1".toByteArray()))
        return headers
    }

    @ParameterizedTest
    @MethodSource("kafkaTests")
    fun `test kafka topology`(params: KafkaTestParams) {
        val settings = App.defineSettings("test${params.count}.yml")
        val testDriver =
            TopologyTestDriver(KafkaTopology(settings).prepare().build(), settings.kafkaStreamsSettings)
        val inputTopic = testDriver.createInputTopic(settings.inputTopic, StringSerializer(), StringSerializer())
        val configInputTopic = testDriver.createInputTopic(
            settings.appSettings.getProperty(Service.CONFIG_TOPIC_PROP_NAME), StringSerializer(), StringSerializer()
        )
        val outputTopic = testDriver.createOutputTopic(settings.outputTopic, StringDeserializer(), StringDeserializer())
        val outputReportTopic =
            testDriver.createOutputTopic(settings.processReportTopic, StringDeserializer(), StringDeserializer())
        val input = readFile("${params.count}/input.nt")

        // The order is important! Otherwise, a race condition could occur in the tests!!!
        configInputTopic.pipeInput(
            "test-record-set-id#localTransform",
            readFile("${params.count}/configs/localTransforms.yml")
        )
        inputTopic.pipeInput(
            TestRecord(params.key, input, createHeaders())
        )

        assertThat(outputTopic.queueSize)
            .isEqualTo(1)

        val reportRecord = outputReportTopic.readRecord()
        assertThat(Report.fromJson(reportRecord.value()))
            .isEqualTo(params.report)


//        val record = outputTopic.readRecord()
//        assertThat(record.value)
//            .isEqualTo(readFile("${params.count}/output.nt"))
    }

    private fun kafkaTests() = Stream.of(
        KafkaTestParams(
            1,
            "test-key-1",
            Report(
                "test-key-1", "WARNING", "The date value 'ca. 1970 er - 1980 er' could not be normalized.\n" +
                        "The date value '(Keine Datumsangabe)' could not be normalized.[Local Transform] Ignored empty local transformation file.",
                "test",
                "appVersion"
            )
        ),
        KafkaTestParams(
            2,
            "placeholder",
            Report(
                "placeholder",
                "SUCCESS",
                "",
                "test",
                "appVersion"
            )
        ),
        KafkaTestParams(
            3,
            "placeholder",
            Report(
                "placeholder", "SUCCESS", "[Local Transform] Ignored empty local transformation file.",
                "test",
                "appVersion"
            )
        ),
        KafkaTestParams(
            4,
            "placeholder",
            Report("placeholder", "SUCCESS", "", "test", "appVersion")
        ),
        KafkaTestParams(
            5,
            "placeholder",
            Report("placeholder", "WARNING", "The date value '25465' could not be normalized.", "test",
                "appVersion")
        ),
        KafkaTestParams(
            6,
            "placeholder",
            Report(
                "placeholder", "SUCCESS", "[Local Transform] Ignored empty local transformation file.",
                "test",
                "appVersion"
            )
        ),
//        KafkaTestParams(
//            7,
//            "placeholder",
//            Report(
//                "placeholder", "WARNING", "No matches found for value Europe occidentale.",
//                "test"
//            )
//        )
    )
}

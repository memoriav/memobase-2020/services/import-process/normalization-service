/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.schema.global.NormalizeLanguages
import ch.memobase.rdf.MemobaseModel
import ch.memobase.rdf.NS
import ch.memobase.rdf.RICO
import java.io.FileOutputStream
import org.apache.jena.riot.RDFDataMgr
import org.apache.jena.riot.RDFFormat
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import java.io.File

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class TestLanguageNormalizer {

    @BeforeAll
    fun setupTest() {
        val tmpFiles = File("src/test/resources/tmp")
        if (!tmpFiles.exists()) {
            tmpFiles.mkdir()
        }
    }

    @Test
    fun `test language normalizer`() {
        val memobaseModel = MemobaseModel()
        NS.prefixMapping.map {
            memobaseModel.setNsPrefix(it.key, it.value)
        }
        val language =
            memobaseModel.createRicoResource(RICO.Language)
                .addLiteral(RICO.name, "Deutsch")
                .addLiteral(RICO.type, "caption")

        val record = memobaseModel.createRicoResource(RICO.Record)
            .addProperty(RICO.hasOrHadLanguage, language)

        val n = NormalizeLanguages(
            "src/test/resources/facets/languages.csv",
            "src/test/resources/facets/language_labels.csv"
        )
        val transform = n.generate()
        val output = transform.transform(language, record, memobaseModel)
        RDFDataMgr.write(
            FileOutputStream("src/test/resources/tmp/turtle-output-language-normalization.ttl"),
            memobaseModel,
            RDFFormat.TURTLE
        )
        assertAll("",
            {
                assertThat(output).isEmpty()
            },
            {
                assertThat(record)
                    .isNotNull

            }
        )
    }
    @Test
    fun `test language normalization with facet value`() {
        val memobaseModel = MemobaseModel()
        NS.prefixMapping.map {
            memobaseModel.setNsPrefix(it.key, it.value)
        }
        val language =
            memobaseModel.createRicoResource(RICO.Language)
                .addLiteral(RICO.name, "A-ssd")
                .addLiteral(RICO.type, "caption")

        val record = memobaseModel.createRicoResource(RICO.Record)
            .addProperty(RICO.hasOrHadLanguage, language)

        val n = NormalizeLanguages(
            "src/test/resources/facets/languages.csv",
            "src/test/resources/facets/language_labels.csv"
        )
        val transform = n.generate()
        val output = transform.transform(language, record, memobaseModel)
        RDFDataMgr.write(
            FileOutputStream("src/test/resources/tmp/turtle-output-language-with-facet-value.ttl"),
            memobaseModel,
            RDFFormat.TURTLE
        )
        assertAll("",
            {
                assertThat(output).isEmpty()
            },
            {
                assertThat(record)
                    .isNotNull

            }
        )
    }
}
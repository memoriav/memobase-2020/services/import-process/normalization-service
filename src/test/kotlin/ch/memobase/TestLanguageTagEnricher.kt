/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.schema.local.EnrichLanguageTags
import ch.memobase.rdf.MemobaseModel
import ch.memobase.rdf.RICO
import ch.memobase.rdf.RicoResource
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestLanguageTagEnricher {

    @Test
    fun `test tag is added to title field from default properties list`() {
        val source = EnrichLanguageTags(
            "de", mapOf(), mapOf()
        )
        val enricher = source.generate()

        val model = MemobaseModel()
        val record = RicoResource(model.createResource(), RICO.Record)
        record.addLiteral(RICO.title, "test-certs")
        enricher.transform(record, record, model)
        assertAll(
            {
                assert(model.contains(record.resource, RICO.title, model.createLiteral("test-certs", "de")))
            },
            {
                assert(!model.contains(record.resource, RICO.title, model.createLiteral("test-certs",)))
            }
        )
    }

    @Test
    fun `test tag is not added if already a tag is present`() {
        val source = EnrichLanguageTags(
            "de", mapOf(), mapOf()
        )
        val enricher = source.generate()

        val model = MemobaseModel()
        val record = RicoResource(model.createResource(), RICO.Record)
        record.addLiteral(RICO.title, "test-certs", "fr")
        enricher.transform(record, record, model)
        assertAll(
            {
                assert(!model.contains(record.resource, RICO.title, model.createLiteral("test-certs", "de")))
            },
            {
                assert(model.contains(record.resource, RICO.title, model.createLiteral("test-certs", "fr")))
            }
        )
    }

    @Test
    fun `test language tag is added for property added from inclusive list`() {
        val source = EnrichLanguageTags(
            "de", mapOf(
                "rico:Record" to listOf(
                    "rico:name"
                )
            ), mapOf()
        )
        val enricher = source.generate()

        val model = MemobaseModel()
        val record = RicoResource(model.createResource(), RICO.Record)
        record.addLiteral(RICO.name, "test-certs")
        enricher.transform(record, record, model)
        assertAll(
            {
                assert(model.contains(record.resource, RICO.name, model.createLiteral("test-certs", "de")))
            },
            {
                assert(!model.contains(record.resource, RICO.name, model.createLiteral("test-certs")))
            }
        )
    }

    @Test
    fun `test language tag is not added for property added to exclusion list`() {
        val source = EnrichLanguageTags(
            "de", mapOf(), mapOf(
                "rico:Record" to listOf(
                    "rico:name"
                )
            )
        )
        val enricher = source.generate()

        val model = MemobaseModel()
        val record = RicoResource(model.createResource(), RICO.Record)
        record.addLiteral(RICO.name, "test-certs")
        enricher.transform(record, record, model)
        assertAll(
            {
                assert(!model.contains(record.resource, RICO.name, model.createLiteral("test-certs", "de")))
            },
            {
                assert(model.contains(record.resource, RICO.name, model.createLiteral("test-certs")))
            }
        )
    }

    @Test
    fun `test language tag is not added for property added to exclusion list that is in default list`() {
        val source = EnrichLanguageTags(
            "de", mapOf(), mapOf(
                "rico:Record" to listOf(
                    "rico:title"
                )
            )
        )
        val enricher = source.generate()

        val model = MemobaseModel()
        val record = RicoResource(model.createResource(), RICO.Record)
        record.addLiteral(RICO.title, "test-certs")
        enricher.transform(record, record, model)
        assertAll(
            {
                assert(!model.contains(record.resource, RICO.title, model.createLiteral("test-certs", "de")))
            },
            {
                assert(model.contains(record.resource, RICO.title, model.createLiteral("test-certs")))
            }
        )
    }
}
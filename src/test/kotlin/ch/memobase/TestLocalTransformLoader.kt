/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.configs.LocalTransformsLoader
import ch.memobase.helpers.RequestClient
import ch.memobase.reporting.ReportStatus
import org.apache.logging.log4j.LogManager
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS
import org.junit.jupiter.api.assertAll
import java.io.File
import javax.net.ssl.SSLContext


@TestInstance(PER_CLASS)
class TestLocalTransformLoader {
    private val log = LogManager.getLogger(this::class.java)

    private fun readYml(fileName: String): ByteArray {
        return File("src/test/resources/local/$fileName.yml").readBytes()
    }

    @Test
    fun `test empty local transform`() {
        val localTransformsLoader = LocalTransformsLoader(
            byteArrayOf(),
            "test-certs",
            "stepVersion",
            requestClient = RequestClient(
                "none",
                "none",
                "none",
                SSLContext.getDefault()
            )
        )
        val report = localTransformsLoader.parse("key")
        assertThat(report.status)
            .isEqualTo(ReportStatus.ignored)
    }

    @Test
    fun `test yml parse error`() {
        val localTransformsLoader = LocalTransformsLoader(
            readYml("invalid"), "test-certs",
            "stepVersion",
            requestClient = RequestClient(
                "none",
                "none",
                "none",
                SSLContext.getDefault()
            )
        )
        val report = localTransformsLoader.parse("key")
        assertAll(
            "",
            {
                assertThat(report.status)
                    .isEqualTo(ReportStatus.fatal)
            },
            {
                assertThat(report.message)
                    .isEqualTo(
                        "[Local Transform] YamlParseError (IncorrectTypeException): Expected an object, but got a scalar value"
                    )
            }
        )
    }

    @Test
    fun `test valid input`() {
        val localTransformsLoader = LocalTransformsLoader(
            readYml("valid"), "test-certs",
            "stepVersion",
            requestClient = RequestClient(
                "none",
                "none",
                "none",
                SSLContext.getDefault()
            )
        )
        val report = localTransformsLoader.parse("key")
        assertAll(
            "",
            {
                assertThat(report.status)
                    .isEqualTo(ReportStatus.success)
            },
            {
                assertThat(report.message)
                    .isEqualTo("")
            }
        )
    }

    @Test
    fun `test invalid entity splitter input`() {
        val localTransformsLoader = LocalTransformsLoader(
            readYml("invalidEntitySplitter"), "test-certs",
            "stepVersion",
            requestClient = RequestClient(
                "none",
                "none",
                "none",
                SSLContext.getDefault()
            )
        )
        val report = localTransformsLoader.parse("key")
        assertAll(
            "",
            {
                assertThat(report.status)
                    .isEqualTo(ReportStatus.fatal)
            },
            {
                assertThat(report.message)
                    .isEqualTo("[Local Transform] ValidationError: Property 'type' should have format 'prefix:ClassName', but was 'ricoPerson' instead.")
            }
        )
    }

    @Test
    fun `test valid language tag enricher input with just a language tag`() {
        val localTransformsLoader = LocalTransformsLoader(
            readYml("languageTagEnricher01"), "test-certs",
            "stepVersion",
            requestClient = RequestClient(
                "none",
                "none",
                "none",
                SSLContext.getDefault()
            )
        )
        val report = localTransformsLoader.parse("key")
        assertAll(
            "",
            {
                assertThat(report.status)
                    .isEqualTo(ReportStatus.success)
            },
            {
                assertThat(report.message)
                    .isEqualTo("")
            }
        )
    }

    @Test
    fun `test valid language tag enricher input with inclusion properties`() {
        val localTransformsLoader = LocalTransformsLoader(
            readYml("languageTagEnricher02"), "test-certs",
            "stepVersion",
            requestClient = RequestClient(
                "none",
                "none",
                "none",
                SSLContext.getDefault()
            )
        )
        val report = localTransformsLoader.parse("key")
        assertAll(
            "",
            {
                assertThat(report.status)
                    .isEqualTo(ReportStatus.success)
            },
            {
                assertThat(report.message)
                    .isEqualTo("")
            }
        )
    }

    @Test
    fun `test valid language tag enricher input with exclusion properties`() {
        val localTransformsLoader = LocalTransformsLoader(
            readYml("languageTagEnricher03"), "test-certs",
            "stepVersion",
            requestClient = RequestClient(
                "none",
                "none",
                "none",
                SSLContext.getDefault()
            )
        )
        val report = localTransformsLoader.parse("key")
        assertAll(
            "",
            {
                assertThat(report.status)
                    .isEqualTo(ReportStatus.success)
            },
            {
                assertThat(report.message)
                    .isEqualTo("")
            }
        )
    }

    @Test
    fun `test invalid include for language tag enricher`() {
        val localTransformsLoader = LocalTransformsLoader(
            readYml("languageTagEnricher04"), "test-certs",
            "stepVersion",
            requestClient = RequestClient(
                "none",
                "none",
                "none",
                SSLContext.getDefault()
            )
        )
        val report = localTransformsLoader.parse("key")
        assertAll(
            "",
            {
                assertThat(report.status)
                    .isEqualTo(ReportStatus.fatal)
            },
            {
                assertThat(report.message)
                    .isEqualTo("[Local Transform] ValidationError: Mapping key is missing a prefix to map to a resource. Correct syntax is 'prefix:ClassName'. Found 'Record' instead.")
            }
        )
    }

    @Test
    fun `test invalid exclude for language tag enricher`() {
        val localTransformsLoader = LocalTransformsLoader(
            readYml("languageTagEnricher05"), "test-certs",
            "stepVersion",
            requestClient = RequestClient(
                "none",
                "none",
                "none",
                SSLContext.getDefault()
            )
        )
        val report = localTransformsLoader.parse("key")
        assertAll(
            "",
            {
                assertThat(report.status)
                    .isEqualTo(ReportStatus.fatal)
            },
            {
                assertThat(report.message)
                    .isEqualTo("[Local Transform] ValidationError: Tried to exclude resource 'Record' from default list, but it is not present. Exclude can only remove properties from resources that are in the default list. [rico:Record, rico:Title, rico:Agent, rico:CorporateBody, rico:Person, rico:CreationRelation, rico:Language, rico:CarrierType, skos:Concept, rico:Instantiation]}")
            }
        )
    }
}
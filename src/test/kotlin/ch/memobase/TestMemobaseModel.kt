/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.rdf.MemobaseModel
import org.apache.jena.riot.Lang
import org.apache.jena.riot.RDFDataMgr
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS
import org.junit.jupiter.api.assertAll
import java.io.File

@TestInstance(PER_CLASS)
class TestMemobaseModel {

    @Test
    fun `test record id filter`() {
        val model = MemobaseModel()
        RDFDataMgr.read(model, File("src/test/resources/memobase-model/input.nt").inputStream(), Lang.NTRIPLES)

        val id = model.getRecordId()

        assertAll("record id filter",
            { assertThat(id).isEqualTo("rtr-004-28a50d65-07bc-4efb-8838-2d7ecce87809_02")},
        )
    }
}
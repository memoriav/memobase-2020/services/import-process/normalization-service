/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.rdf.MemobaseModel
import ch.memobase.rdf.NS
import ch.memobase.rdf.RDA
import ch.memobase.rdf.RICO
import ch.memobase.rdf.RicoResource
import ch.memobase.transform.PersonNormalizer
import java.io.FileOutputStream
import java.util.stream.Stream
import org.apache.jena.riot.RDFDataMgr
import org.apache.jena.riot.RDFFormat
import org.apache.jena.sparql.vocabulary.FOAF
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import ch.memobase.params.PersonNormalizerParams
import org.junit.jupiter.api.BeforeAll
import java.io.File

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class TestPersonNormalizer {

    @BeforeAll
    fun setupTest() {
        val tmpFiles = File("src/test/resources/tmp")
        if (!tmpFiles.exists()) {
            tmpFiles.mkdir()
        }
    }

    private fun createModel(name: String): Pair<RicoResource, MemobaseModel> {
        val memobaseModel = MemobaseModel()
        NS.prefixMapping.map {
            memobaseModel.setNsPrefix(it.key, it.value)
        }
        val resource =
                memobaseModel.createRicoResource(RICO.Person)
                        .addLiteral(RICO.name, name)
        memobaseModel.createRicoResource(RICO.Record)
                .addProperty(RDA.hasProducer, resource)
        return Pair(resource, memobaseModel)
    }

    private fun write(name: String, model: MemobaseModel) {
        RDFDataMgr.write(
                FileOutputStream("src/test/resources/tmp/${name}.ttl"),
                model,
                RDFFormat.TURTLE_PRETTY
        )
    }

    @Test
    fun `test input with both first and last name`() {
        val source = createModel("Markus Mäder")
        val n = PersonNormalizer(
                "first-to-last",
                true,
                " "
        )
        val output = n.transform(source.first, RicoResource(source.second.createResource(), RICO.Record), source.second)
        write("both_names", source.second)
        assertAll("",
                {
                    assertThat(output)
                            .isEmpty()
                },
                {
                    assertThat(source.first.hasProperty(FOAF.firstName, "Markus")).isTrue()
                },
                {
                    assertThat(source.first.hasProperty(FOAF.lastName, "Mäder")).isTrue()
                }
        )
    }

    @Test
    fun `test input with only first name`() {
        val source = createModel("Markus")
        val n = PersonNormalizer(
                "last-to-first",
                false,
                " "
        )
        val output = n.transform(source.first,RicoResource(source.second.createResource(), RICO.Record), source.second)
        write("first_name_only", source.second)
        assertAll("",
                {
                    assertThat(output).isEmpty()
                },
                {
                    assertThat(source.first.hasProperty(FOAF.firstName, "Markus")).isTrue()
                },
                {
                    assertThat(source.first.hasProperty(FOAF.lastName)).isFalse()
                }
        )
    }

    @Test
    fun `test input with only last name`() {
        val source = createModel("Mäder")
        val n = PersonNormalizer(
                "first-to-last",
                true,
                " "
        )
        val output = n.transform(source.first, RicoResource(source.second.createResource(), RICO.Record), source.second)
        write("last_name_only", source.second)
        assertAll("",
                {
                    assertThat(output).isEmpty()
                },
                {
                    assertThat(source.first.hasProperty(FOAF.lastName, "Mäder")).isTrue()
                },
                {
                    assertThat(source.first.hasProperty(FOAF.firstName)).isFalse()
                }
        )
    }


    @ParameterizedTest
    @MethodSource("personNormalizerParams")
    fun `test person normalizer`(params: PersonNormalizerParams) {
        val normalizer = PersonNormalizer(params.nameOrder, params.singleNameIsLastName, params.nameDelimiter)

        val model = MemobaseModel()
        val person =
            model.createRicoResource(RICO.Person)
                .addLiteral(RICO.name, params.name)

        val result = normalizer.transform(person, RicoResource(model.createResource(), RICO.Record), model)

        assertAll("person normalizer tests",
            { assertThat(person.hasProperty(FOAF.firstName, params.firstName)).isEqualTo(params.hasFirstName) },
            { assertThat(person.hasProperty(FOAF.lastName, params.lastName)).isEqualTo(params.hasLastName) },
            { assertThat(result).isEmpty() }
        )
    }

    fun personNormalizerParams(): Stream<PersonNormalizerParams> = Stream.of(
        PersonNormalizerParams(
            "last-to-first",
            true,
            ",",
            "Vogel, Peter",
            "Peter",
            true,
            "Vogel",
            true
        ),
        PersonNormalizerParams(
            "first-to-last",
            true,
            " ",
            "Peter Vogel",
            "Peter",
            true,
            "Vogel",
            true
        ),
        PersonNormalizerParams(
            "first-to-last",
            true,
            " ",
            "Peter Hans Vogel",
            "Peter Hans",
            true,
            "Vogel",
            true
        ),
        PersonNormalizerParams(
            "first-to-last",
            true,
            " ",
            "Vogel",
            "",
            false,
            "Vogel",
            true
        ),
        PersonNormalizerParams(
            "first-to-last",
            false,
            " ",
            "Peter",
            "Peter",
            true,
            "",
            false
        )
    )
}
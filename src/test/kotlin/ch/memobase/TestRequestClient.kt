/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.helpers.RequestClient
import ch.memobase.helpers.getSSLContext
import ch.memobase.schema.external.WikidataLinkerValueType
import ch.memobase.rdf.WD
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll

// only used locally as the services are currently not available in the CI/CD pipeline
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Disabled
class TestRequestClient {

    private val placeProperties =
        listOf(
            WD.INSTANCE_OF_P31,
            WD.GND_ID_P227,
            WD.COORDINATE_LOCATION_P625,
            WD.HDS_ID_P902,
            WD.COORDINATES_OF_GEOGRAPHIC_CENTER_P5140,
        )

    private val personProperties = listOf(
        WD.INSTANCE_OF_P31,
        WD.GND_ID_P227,
        WD.HDS_ID_P902,
    )

    @Test
    fun `test check health`() {
        val sslContext = getSSLContext(
            System.getenv("CA_CERT"),
            System.getenv("CLIENT_CERT"),
            System.getenv("CLIENT_KEY"),
        )
        val wikidataClient = RequestClient(
            "https://wikidata-badger.ub-dd-prod.k8s.unibas.ch/api/v1",
            "https://localhost:3000/link",
            "https://localhost:3010/report",
            sslContext
        )

        wikidataClient.checkHealth()
    }


    @Test
    fun `test wikidata linker`() {
        val sslContext = getSSLContext(
            System.getenv("CA_CERT"),
            System.getenv("CLIENT_CERT"),
            System.getenv("CLIENT_KEY"),
        )
        val wikidataClient = RequestClient(
            "https://wikidata-badger.ub-dd-prod.k8s.unibas.ch/api/v1",
            "https://localhost:3000/link",
            "https://localhost:3010/report",
            sslContext
        )
        val response = wikidataClient.requestWikidataLink("Glarus-Süd, GL (Schweiz), Europa", "bla-001",  WikidataLinkerValueType.LOCATION)

        assertAll(
            { assertThat(response.value).isEqualTo("Glarus-Süd, GL (Schweiz), Europa") },
            { assertThat(response.extractor).isEqualTo("pattern(^(?<mun>[-\\p{Latin}\\. /]+), (?<staa>[A-Z]{2}) \\((?<cou>[-\\p{Latin}]+)\\), (?<con>[\\p{Latin} ]+)\$)") },
            { assertThat(response.matches)
                .isNotNull.hasSize(3) }
        )
    }

    @Test
    fun `test wikidata service`() {
        val sslContext = getSSLContext(
            System.getenv("CA_CERT"),
            System.getenv("CLIENT_CERT"),
            System.getenv("CLIENT_KEY"),
        )
        val wikidataClient = RequestClient(
            "https://wikidata-badger.ub-dd-prod.k8s.unibas.ch/api/v1",
            "https://localhost:3000/link",
            "https://localhost:3010/report",
            sslContext
        )

        val response = wikidataClient.requestWikidataMetadata(42, placeProperties)

        assertAll(
            { assertThat(response).isNotNull },
            { assertThat(response.error).isNull() }
        )
    }

    @Test
    fun `test connectom service - no result`() {
        val sslContext = getSSLContext(
            System.getenv("CA_CERT"),
            System.getenv("CLIENT_CERT"),
            System.getenv("CLIENT_KEY"),
        )
        val connectomClient = RequestClient(
            "https://connectom.ub-dd-prod.k8s.unibas.ch/api/v1",
            "https://localhost:3000/link",
            "https://localhost:3010/record",
            sslContext
        )

        val response = connectomClient.requestConnectomData("bsa-001.121221")

        assertAll(
            { assertThat(response.id).isEqualTo("No matches found for value bsa-001.121221.")},
        )
    }

    @Test
    fun `test connectom service - with result`() {
        val sslContext = getSSLContext(
            System.getenv("CA_CERT"),
            System.getenv("CLIENT_CERT"),
            System.getenv("CLIENT_KEY"),
        )
        val connectomClient = RequestClient(
            "https://connectom.ub-dd-prod.k8s.unibas.ch/api/v1",
            "https://localhost:3000/link",
            "https://localhost:3010/record",
            sslContext
        )

        val response = connectomClient.requestConnectomData("bar-001-CJS_0009-2")
        assertAll(
            { assertThat(response.id).isEqualTo("bar-001-CJS_0009-2") }
        )
    }

    @Test
    fun `test connectom service - specific result`() {
        val sslContext = getSSLContext(
            System.getenv("CA_CERT"),
            System.getenv("CLIENT_CERT"),
            System.getenv("CLIENT_KEY"),
        )
        val connectomClient = RequestClient(
            "https://connectom.ub-dd-prod.k8s.unibas.ch/api/v1",
            "https://localhost:3000/link",
            "https://localhost:3010/record",
            sslContext
        )

        val response = connectomClient.requestConnectomData("bar-001-SFW_0104-2")
        assertAll(
            { assertThat(response.id).isEqualTo("bar-001-SFW_0104-2") }
        )
    }
}
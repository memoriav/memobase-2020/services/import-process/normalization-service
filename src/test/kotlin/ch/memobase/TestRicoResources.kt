/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.rdf.MemobaseModel
import ch.memobase.rdf.RICO
import org.apache.jena.rdf.model.Statement
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestRicoResources {

    @Test
    fun `test remove literal statement`() {
        val memobaseModel = MemobaseModel()

        val person = memobaseModel.createRicoResource(RICO.Person)
            .addLiteral(RICO.name, "TEST")
            .addLiteral(RICO.name, "KEEP")

        person.removeAllProperties(RICO.name, "TEST")

        assertThat(person)
            .extracting { it.listProperties(RICO.name) }
            .asList()
            .allMatch {
                it as Statement
                it.string == "KEEP"
            }
    }

    @Test
    fun `test remove no matching literal statement`() {
        val memobaseModel = MemobaseModel()

        val person = memobaseModel.createRicoResource(RICO.Person)
            .addLiteral(RICO.name, "KEEP1")
            .addLiteral(RICO.name, "KEEP2")

        person.removeAllProperties(RICO.name, "TEST")

        assertThat(person)
            .extracting { it.listProperties(RICO.name) }
            .asList()
            .allMatch {
                it as Statement
                it.string == "KEEP1" || it.string == "KEEP2"
            }
    }
}

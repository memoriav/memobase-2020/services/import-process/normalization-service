/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.helpers.RequestClient
import ch.memobase.helpers.getSSLContext
import ch.memobase.rdf.*
import ch.memobase.transform.WikidataLinksEnricher
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Disabled
class TestWikidataEnricher {


    @Test
    fun `test free text enrichment`() {
        val sslContext = getSSLContext(
            System.getenv("CA_CERT"),
            System.getenv("CLIENT_CERT"),
            System.getenv("CLIENT_KEY"),
        )
        val wikidataClient = RequestClient(
            "https://wikidata-badger.ub-dd-prod.k8s.unibas.ch/api/v1",
            "https://localhost:3000/link",
            "https://localhost:3010/record",
            sslContext
        )

        val wikidataEnricher = WikidataLinksEnricher("bar-001", wikidataClient)

        val model = MemobaseModel()
        val record = RicoResource(model.createResource("https://memobase.ch/record/bar-001-CJS_0324-2_d"), RICO.Record)
        val identifier = RicoResource(model.createResource(), RICO.Identifier)
        identifier.addLiteral(RICO.identifier, "bar-001-CJS_0324-2_d")
        identifier.addLiteral(RICO.type, RICO.Types.Identifier.main)
        identifier.addProperty(RICO.isOrWasIdentifierOf, record)
        record.addProperty(RICO.hasOrHadIdentifier, identifier)
        wikidataEnricher.enrichFreetextEntities(record, model)

        println(model.toString())
    }


    @Test
    fun `test wikidata linking`() {
        val sslContext = getSSLContext(
            System.getenv("CA_CERT"),
            System.getenv("CLIENT_CERT"),
            System.getenv("CLIENT_KEY"),
        )
        val wikidataClient = RequestClient(
            "https://wikidata-badger.ub-dd-prod.k8s.unibas.ch/api/v1",
            "https://localhost:3000/link",
            "https://localhost:3010/record",
            sslContext
        )

        val wikidataEnricher = WikidataLinksEnricher("bar-001", wikidataClient)

        val model = MemobaseModel()
        val record = RicoResource(model.createResource("https://memobase.ch/record/bar-001-CJS_0001-4-1"), RICO.Record)
        val identifier = RicoResource(model.createResource(), RICO.Identifier)
        identifier.addLiteral(RICO.identifier, "bar-001-CJS_0001-4-1")
        identifier.addLiteral(RICO.type, RICO.Types.Identifier.main)
        identifier.addProperty(RICO.isOrWasIdentifierOf, record)
        record.addProperty(RICO.hasOrHadIdentifier, identifier)

        val place = RicoResource(model.createResource(), RICO.Place)
        place.addLiteral(RICO.name, "Bern, BE (Schweiz), Europa")
        record.addProperty(RDA.hasPlaceOfCapture, place)
        wikidataEnricher.transform(place, record, model)
        println(model.toString())
    }



    @Test
    fun `test wikidata linking - two`() {
        val sslContext = getSSLContext(
            System.getenv("CA_CERT"),
            System.getenv("CLIENT_CERT"),
            System.getenv("CLIENT_KEY"),
        )
        val wikidataClient = RequestClient(
            "https://wikidata-badger.ub-dd-prod.k8s.unibas.ch/api/v1",
            "https://localhost:3000/link",
            "https://localhost:3010/record",
            sslContext
        )

        val wikidataEnricher = WikidataLinksEnricher("bar-001", wikidataClient)

        val model = MemobaseModel()
        val record = RicoResource(model.createResource("https://memobase.ch/record/bar-001-SFW_0104-2"), RICO.Record)
        val identifier = RicoResource(model.createResource(), RICO.Identifier)
        identifier.addLiteral(RICO.identifier, "bar-001-SFW_0104-2")
        identifier.addLiteral(RICO.type, RICO.Types.Identifier.main)
        identifier.addProperty(RICO.isOrWasIdentifierOf, record)
        record.addProperty(RICO.hasOrHadIdentifier, identifier)

        val place = RicoResource(model.createResource(), RICO.Place)
        place.addLiteral(RICO.name, "Jungfraujoch, VS (Schweiz)")
        record.addProperty(RDA.hasPlaceOfCapture, place)
        wikidataEnricher.transform(place, record, model)
        println(model.toString())
    }

    @Test
    fun `test wikidata linking and enrichment`() {
        val sslContext = getSSLContext(
            System.getenv("CA_CERT"),
            System.getenv("CLIENT_CERT"),
            System.getenv("CLIENT_KEY"),
        )
        val wikidataClient = RequestClient(
            "https://wikidata-badger.ub-dd-prod.k8s.unibas.ch/api/v1",
            "https://localhost:3000/link",
            "https://localhost:3010/record",
            sslContext
        )

        val wikidataEnricher = WikidataLinksEnricher("bar-001", wikidataClient)

        val model = MemobaseModel()
        val record = RicoResource(model.createResource("https://memobase.ch/record/bar-001-SFW_1524-1"), RICO.Record)
        val identifier = RicoResource(model.createResource(), RICO.Identifier)
        identifier.addLiteral(RICO.identifier, "bar-001-SFW_1524-1")
        identifier.addLiteral(RICO.type, RICO.Types.Identifier.main)
        identifier.addProperty(RICO.isOrWasIdentifierOf, record)
        record.addProperty(RICO.hasOrHadIdentifier, identifier)

        val place = RicoResource(model.createResource(), RICO.Place)
        place.addLiteral(RICO.name, "Kilchberg, ZH (Schweiz)")
        record.addProperty(RDA.hasPlaceOfCapture, place)
        wikidataEnricher.enrichFreetextEntities(record, model)
        wikidataEnricher.transform(place, record, model)
        println(model.toString())
    }

    @Test
    fun `test wikidata linking and enrichment  -  bar-001-SFW_0404-1`() {
        val sslContext = getSSLContext(
            System.getenv("CA_CERT"),
            System.getenv("CLIENT_CERT"),
            System.getenv("CLIENT_KEY"),
        )
        val wikidataClient = RequestClient(
            "https://wikidata-badger.ub-dd-prod.k8s.unibas.ch/api/v1",
            "https://localhost:3000/link",
            "https://localhost:3010/record",
            sslContext
        )

        val wikidataEnricher = WikidataLinksEnricher("bar-001", wikidataClient)
        val model = MemobaseModel()
        val record = RicoResource(model.createResource("https://memobase.ch/record/bar-001-SFW_0404-1"), RICO.Record)
        val identifier = RicoResource(model.createResource(), RICO.Identifier)
        identifier.addLiteral(RICO.identifier, "bar-001-SFW_0404-1")
        identifier.addLiteral(RICO.type, RICO.Types.Identifier.main)
        identifier.addProperty(RICO.isOrWasIdentifierOf, record)
        record.addProperty(RICO.hasOrHadIdentifier, identifier)
        wikidataEnricher.enrichFreetextEntities(record, model)
        println(model.toString())
    }


    @Test
    fun `test wikidata linker - agent`() {
        val sslContext = getSSLContext(
            System.getenv("CA_CERT"),
            System.getenv("CLIENT_CERT"),
            System.getenv("CLIENT_KEY"),
        )
        val wikidataClient = RequestClient(
            "https://wikidata-badger.ub-dd-prod.k8s.unibas.ch/api/v1",
            "https://localhost:3000/link",
            "https://localhost:3010/record",
            sslContext
        )

        val wikidataEnricher = WikidataLinksEnricher("bar-001", wikidataClient)

        val model = MemobaseModel()
        val record = RicoResource(model.createResource("https://memobase.ch/record/bar-001-SFW_1524-1"), RICO.Record)
        val identifier = RicoResource(model.createResource(), RICO.Identifier)
        identifier.addLiteral(RICO.identifier, "bar-001-SFW_1524-1")
        identifier.addLiteral(RICO.type, RICO.Types.Identifier.main)
        identifier.addProperty(RICO.isOrWasIdentifierOf, record)
        record.addProperty(RICO.hasOrHadIdentifier, identifier)

        val person = RicoResource(model.createResource(), RICO.Person)
        person.addLiteral(FOAF.firstName, "Henri")
        person.addLiteral(FOAF.lastName, "Guisan")
        person.addLiteral(RICO.name, "Henri Guisan")
        record.addProperty(RICO.hasOrHadSubject, person)
        wikidataEnricher.transform(person, record, model)

        println(model.toString())
    }


    @Test
    fun `test wikidata has capture of link`() {
        val sslContext = getSSLContext(
            System.getenv("CA_CERT"),
            System.getenv("CLIENT_CERT"),
            System.getenv("CLIENT_KEY"),
        )
        val wikidataClient = RequestClient(
            "https://wikidata-badger.ub-dd-prod.k8s.unibas.ch/api/v1",
            "https://localhost:3000/link",
            "https://localhost:3010/record",
            sslContext
        )

        val wikidataEnricher = WikidataLinksEnricher("bar-001", wikidataClient)

        val model = MemobaseModel()
        val record = RicoResource(model.createResource("https://memobase.ch/record/bar-001-SFW_1634-1"), RICO.Record)
        val identifier = RicoResource(model.createResource(), RICO.Identifier)
        identifier.addLiteral(RICO.identifier, "bar-001-SFW_1524-1")
        identifier.addLiteral(RICO.type, RICO.Types.Identifier.main)
        identifier.addProperty(RICO.isOrWasIdentifierOf, record)
        record.addProperty(RICO.hasOrHadIdentifier, identifier)

        val placeOfCapture = RicoResource(model.createResource(), RICO.Place)
        placeOfCapture.addLiteral(RICO.name, "Avenches, VD (Schweiz)")
        record.addProperty(RDA.hasPlaceOfCapture, placeOfCapture)

        val spatial = RicoResource(model.createResource(), RICO.Place)
        spatial.addLiteral(RICO.name, "Westeuropa")
        record.addProperty(DC.spatial, spatial)

        wikidataEnricher.enrichFreetextEntities(record, model)
        wikidataEnricher.transform(placeOfCapture, record, model)

        println(model.toString())
    }
}
/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.helpers.Values
import ch.memobase.helpers.WikidataToMemobase
import ch.memobase.rdf.MemobaseModel
import ch.memobase.rdf.RICO
import ch.memobase.schema.external.WikidataServiceResponseBody
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromStream
import org.assertj.core.api.AssertionsForClassTypes.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import java.io.File


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestWikidataToMemobase {

    @OptIn(ExperimentalSerializationApi::class)
    @Test
    fun `test wikidata metadata rdf transform`() {
        val issues = mutableListOf<String>()
        val values = WikidataToMemobase.transformResponse(
            MemobaseModel(),
            RICO.Place,
            Values.ENTITY_RICO_TYPE_ENRICHED_BY_MEMOBASE,
            Json.decodeFromStream(
                WikidataServiceResponseBody.serializer(),
                File("src/test/resources/wikidata-responses/Q3060742.json").inputStream()
            ),
            issues,
            3060742,
        )

        assertAll(
            {
                assertThat(values)
                    .isNull()
            },
            {
                assertThat(issues.size)
                    .isEqualTo(1)
            },
            {
                assertThat(issues.first())
                    .isEqualTo("Found match to Wikimedia disambiguation page (Q3060742). This match was Enriched by Memobase.")
            }
        )
    }
}